#!/usr/bin/php -q
<?php
  require_once ('/var/www/air-access/lib/DatabaseAccessMysql.php');

  $item = $argv[1]; // var_dump($item);
  $data = json_decode($item, true); // var_dump($data);

  //-- パラメータ取得
  $orderId = $data["orderId"];
  $productCode = $data["productCode"];
  $quantity = intval($data["quantity"]);
  
  $dbam = DatabaseAccessMysql::singleton("mysql");
  $dbam->connectToDb();

  //-- トランザクション開始
  $dbam->execSql('begin', array());

  //-- 指定数量分の優待番号を取得
  $res = $dbam->execSql(
/***
    'select * from `dtb_benefit` where `order_id` is null and `product_code` = :productCode and `del_flg` <> 1 limit ' . $quantity . ' FOR UPDATE',
***/
      'select * from `dtb_benefit`'
      . ' where'
      . ' `benefit_code` is not null'
      . ' and `product_code` = :productCode'
      . ' and `exdate_from` <= CURRENT_DATE()'
      . ' and (CURRENT_DATE() + INTERVAL 3 DAY) <= `exdate_to`'
      . ' and `order_id` is null'
      . ' and `done_date` is null'
      . ' and `del_flg` <> 1'
      . ' order by `exdate_to`'
      . ' limit ' . $quantity
      . ' FOR UPDATE',
    array(':productCode' => $productCode)
  );

  //-- 取得出来た数量
  $countBenefit = count($res);

  if($countBenefit == $quantity){
    //-- 指定数量取得出来た場合は、それらレコードの `order_id` をセット
    for($i=0; $i<$countBenefit; $i++){
      $dbam->execSql(
        'update `dtb_benefit` set `order_id` = :orderId, `done_date` = CURRENT_TIMESTAMP() where `benefit_id` = :benefitId and `del_flg` <> 1',
        array(':orderId' => $orderId, ':benefitId' => $res[$i]['benefit_id'])
      );
    }
    $dbam->execSql('commit', array());
  }
  else{
    //-- 指定数量に満たない場合はロールバック
    $dbam->execSql('rollback', array());
    $res = null;
  }
  
  exit(json_encode($res));
?>
