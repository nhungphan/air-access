#!/usr/bin/php -q
<?php
class DatabaseAccessMysql{

//★★★★★★★　PEARLOG は使用しない　★★★★★★★★★★★★★★★★
	//DB接続先
	public $db_target;
	//useMaster()の履歴
	private $previous_targets;
	//トランザクション使えるかのフラグ
	private $committed_only;

	//トランザクションカウント
	private static $transaction_cnt = 0;
	//書き込み用PDOインスタンス
	private static $master_db;
	//読み込み用PDOインスタンス
	private static $slave_db;
	//一般インスタンス
	private static $inst_singleton;
	//読み込み専用インスタンス
	private static $inst_committed;


	//最後のトランザクションのコミット後
	private static $slave_wait_file;
	//レプリケーション情報
	private static $slave_wait_pos;
	//未接続のスレーブ情報
	private static $slave_arr;
	//現在レコードロックしている対象
	public $lock_records_info = array();

	private $user_id;



	//DB接続情報
	private $db_info = array(
	  'pgsql' => array(
	    'dbname' => 'access-ticket',
	    'host' => '127.0.0.1',
	    'port' => '12345',
	    'user' => 'postgres',
	    'pass' => 'IBJkaKlkueKiO8'
	  ),
	  'mysql' => array(
			// 本番環境用DB
			'dbname' => 'air_access',
			// ステージング環境用DB
			// 'dbname' => 'air_access_stg',
	    'host' => '127.0.0.1',
	    'port' => '3306',
	    'user' => 'wakyo',
	    'pass' => 'Wakyo@000'
	  )
	);

  //使用するdbが入る
	public $db_name;

	//*****
	//シングルトン
	//@return DatabaseAccess：このクラスの実体
	static function singleton($db)
	{

 		if (!isset(self::$inst_singleton)) {
			self::$inst_singleton = new DatabaseAccessMysql($db, false);
		}else{
			self::$inst_singleton->db_name = $db;
		}

		return self::$inst_singleton;
	}

	//*****
	//読み込み専用シングルトン
	//未コミットのデータが影響されません
	static function committedOnly($db)
	{
		//$this->db_name = $db;

		if (!isset(self::$inst_committed)) {
			self::$inst_committed = new DatabaseAccessMysql($db, true);
		}else{
			self::$inst_singleton->db_name = $db;
		}

		return self::$inst_committed;
	}


	//*****
	//コンストラクタ
	//@param bool $committed_only：コミットされているデータのみ参照フラグ
	protected function __construct($db, $committed_only)
	{
		$this->committed_only = $committed_only;
/*
		//ログを利用
		$this->logger = CommonFunctions::makeLogger($log);
*/

//$this->db_target = 'SLAVE';
    $this->db_target = 'MASTER';
    $this->db_name = $db;

		$this->previous_targets = array();
	}

	//*****
	//マスターDBを使う
	//トランザクション無しでもマスターを使いたい場合
	//基本的に使った後にusePrevious()も実効
	function useMaster()
	{
		//現在のターゲットを保存しておく
		$this->previous_targets[] = $this->db_target;

		//マスターを既に使っている場合は終わり
		if ('MASTER' == $this->db_target) {
			return;
		}

		//読み込み専用インスタンスでは使えない
		if ($this->committed_only) {
			throw new Exception("この接続ではマスターをアクセスすることができません");
		}

		//ターゲットを設定する
		$this->db_target = 'MASTER';

		//__get()で初期化されるのでdbをクリアしておく
		unset($this->db);
	}


	//*****
	//ターゲットを戻す
	//useMaster()を実効した後にこれを実効
	function usePrevious()
	{
		if (!$this->previous_targets) {
			throw new Exception("useMaster()は呼び出されていません");
		}

		//以前のターゲットを取得
		$previous_target = array_pop($this->previous_targets);

		//変わってれば適用
		if ($previous_target != $this->db_target) {
			$this->db_target = $previous_target;
			unset($this->db);
		}
	}


	//*****
	//初期化されていないメンバー変数を取得
	//このPHP機能を使ってジャストインタイムでDBに接続する
	//初期化されていない配列のアクセス(例：$obj->some_array[] = 1)を動作するために参照型で返す
	//@param string $name：メンバー変数の名前
	//@return mixed：メンバー変数の値
	function & __get($name)
	{
		//メンバー変数によって動作を決める
		switch ($name) {
		//DBの接続が必要
		case 'db':
			//現在のターゲットを見て接続をセットする
			switch ($this->db_target) {
			case 'MASTER':
				$this->db = self::connectToMaster();
				break;

			case 'SLAVE':
				$this->db = self::connectToSlave();
				break;

			default:
				throw new Exception("不明ターゲット：{$this->db_target}");
			}
			break;

		//知らないメンバー変数の動作は__get関数無しと同じ動作にする
		default:
			$this->$name = null;
		}

		//セットしたメンバー変数を返す
		return $this->$name;
	}


	//*****
	//マスターDBに接続する
	//接続はクラス変数でキャッシュする
	//@return PDOインスタンス：DB接続
	private function connectToMaster()
	{
		if (!isset(self::$master_db)) {
			// self::$master_db = self::connectToDb(DB_HOST_MASTER, DB_USER_MASTER, DB_PASS_MASTER);
			self::$master_db = self::connectToDb();
		}

		return self::$master_db;
	}


	//*****
	//スレーブDBに接続する
	//すべてのスレーブがダウンしている場合はマスターの新たに接続する
	//接続はクラス変数でキャッシュする
	//@return PDOインスタンス：DB接続
	private function connectToSlave()
	{
		if (empty(self::$slave_arr)) {
			//スレーブ情報を配列に
			self::$slave_arr = array();
			for ($i = 1; $i <= DB_HOST_SLAVE_NUM; $i++) {
				$host   = constant("DB_HOST_SLAVE{$i}");
				$weight = constant("DB_SWITCH_WEIGHT${i}");
				//重みが0の場合は使わない
				if (0 < $weight) {
					// DBバックアップ処理中の場合は使わない（負荷が高く参照できないため）
					if(!(DB_BACKUP_HOST == $host and
						date('H:i:s') >= DB_BACKUP_START_TIME and
						date('H:i:s') < DB_BACKUP_END_TIME)){
						self::$slave_arr[$host] = $weight;
					}
				}
			}
		}

		do {
			//接続してない場合は未接続のスレーブを選んでつないでみる
			while (!isset(self::$slave_db) && !empty(self::$slave_arr)) {
				//重みを足してランダムでサーバを決める
				$random = rand(1, array_sum(self::$slave_arr));
				foreach (self::$slave_arr as $host => $weight) {
					if ($weight < $random) {
						//ランダムの値が重みより大きい時は次のサーバ
						$random -= $weight;
					} else {
						//このサーバに決定
						unset(self::$slave_arr[$host]);
						break;
					}
				}

				try {
					//接続してみる
					self::$slave_db = self::connectToDb($host, DB_USER_SLAVE, DB_PASS_SLAVE);
				} catch (Exception $e) { }
			}

			//最後のトランザクションがあればスレーブに反映されていることを待つ
			if (isset(self::$slave_db) && isset(self::$slave_wait_file)) {
				//2秒タイムアウトで実行する
				$result = self::$slave_db->query(
					'SELECT MASTER_POS_WAIT("' . self::$slave_wait_file . '", ' . self::$slave_wait_pos . ', 2)')->fetchColumn();
				if (-1 == $result) {
					//MASTER_POS_WAITがタイムアウトしたので、違うスレーブにする
					self::$slave_db = null;
				}
			}
		} while (!isset(self::$slave_db) && !empty(self::$slave_arr));

		//どのスレーブにも接続できなかった場合はマスターを使う
		if (!isset(self::$slave_db)) {
			self::$slave_db = self::connectToDb(DB_HOST_MASTER, DB_USER_SLAVE, DB_PASS_SLAVE);
		}

		return self::$slave_db;
	}


 //*****
 //共通パラメータを設定して指定のDBに接続
 //@param string：ホスト名
 //@param string：ユーザ名
 //@param string：パスワード
 //@return PDOインスタンス：DB接続
 function connectToDb()
 {
	 //データベースに接続する
	//  $db_dsn = echo DB_mysql. ":dbname=". DB_NAME. ";host=". DB_HOST. "port=". DB_PORT_mysql;
	$connect = $this->db_info[$this->db_name];

	$db_dsn = "{$this->db_name}:host={$connect['host']};port={$connect['port']};dbname={$connect['dbname']}";

	 $db_user = $connect['user'];
	 $db_pass = $connect['pass'];

   try{

		  $attribute = array(
// defineを切った方が良い
       //持続的接続
       // PDO::ATTR_PERSISTENT => DB_PERSISTENT,
       PDO::ATTR_PERSISTENT => true,
			 //エンコーディング
       //PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . DB_ENCODING,
       PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . 'utf8',
       //prepare()のエミュレーション
			 PDO::ATTR_EMULATE_PREPARES => true,
			 //接続タイムアウト
       //PDO::ATTR_TIMEOUT => DB_CONNECT_TIMEOUT,
       PDO::ATTR_TIMEOUT => '30'
		 );

      $db = new PDO($db_dsn, $db_user, $db_pass, $attribute);
	    // $db = new PDO($db_dsn, $db_user, $db_pass);

   }catch(Exception $e){
		 echo "[ERROR] ". $e->getMessage() . "\n";
		 die();

		 //PDOの属性設定
		 //エラー時はExceptionを投げる
		//  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		 //バッファクエリを使用
		 //$db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);

	 }
	 return $db;
 }



	//*****
	//全接続を切断する
	//トランザクション中ではできない
	//CLIでのフォーク意外は基本的に使う必要はない
	static function disconnectAll()
	{
		//トランザクション中チェック
		if (self::$transaction_cnt) {
			throw new Exception("トランザクション中で切断できません");
		}

		//インスタンスキャッシュをクリア
		self::$master_db = null;
		self::$slave_db = null;

		//スレーブ情報配列も初期化する
		self::$slave_arr = null;

		//シングルトンの接続をリセット
		//ロガーもDBを使う可能性があるのでそれも一旦閉じる
		if (isset(self::$inst_singleton)) {
			unset(self::$inst_singleton->db);
			self::$inst_singleton->logger->close();
		}

		if (isset(self::$inst_committed)) {
			unset(self::$inst_committed->db);
			self::$inst_committed->logger->close();
		}
	}

	//*****
	//トランザクション開始
	//@param string $logging：「nologging」が指定された場合にはログは記録しない
	function myBeginTransaction($logging = null)
	{
		if (0 == self::$transaction_cnt) {
			$this->useMaster();
			//トランザクション開始
			$this->db->beginTransaction();
		}
		self::$transaction_cnt++;
		if ('nologging' != $logging) {
			$this->logger->log(sprintf('トランザクション開始(%s)', $this->transaction_cnt), PEAR_LOG_DEBUG);
		}
	}

	//*****
	//コミット
	//@param string $logging：'nologging'が指定された場合にはログは記録しない
	function myCommit($logging = null)
	{
		if ('nologging' != $logging) {
			$this->logger->log(sprintf('コミット(%s)', $this->transaction_cnt), PEAR_LOG_DEBUG);
		}
		if (0 < self::$transaction_cnt) {
			self::$transaction_cnt--;
		}
		if (0 == self::$transaction_cnt) {
			//ロック情報をクリア
			$this->lock_records_info = array();
			$this->db->commit();
			//スレーブを確認するため、レプリケーション情報を取っておく
			$stmt = $this->db->query('SHOW MASTER STATUS');
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			self::$slave_wait_file = $row['File'];
			self::$slave_wait_pos  = $row['Position'];
			$this->usePrevious();
		}
	}

	//*****
	//ロールバック
	//@param string $logging：'nologging'が指定された場合にはログは記録しない
	function myRollback($logging = null)
	{
		if ('nologging' != $logging) {
			$this->logger->log(sprintf('ロールバック(%s)', $this->transaction_cnt), PEAR_LOG_DEBUG);
		}
		if (0 < self::$transaction_cnt) {
			self::$transaction_cnt--;
		}
		if (0 == self::$transaction_cnt) {
			//ロック情報をクリア
			$this->lock_records_info = array();
			$this->db->rollback();
			$this->usePrevious();
		}
	}

	//*****
	//Likeエスケープ
	//@param string $keyword：LIKE検索キーワード
	function likeEscape($keyword)
	{
		return strtr($keyword, array('%' => '\%'));
	}

	//ユーザID設定
	public function setUserid($uid)
	{
		$this->user_id = $uid;
	}

 	/**
	 * 汎用セレクト
	 * 汎用的に使える簡易セレクト処理メソッド
	 * @param	string		$table			テーブル名
	 * @param	array		$where			更新条件（WHERE句に利用、ANDで結合）　array('[フィールド名]' => '[条件値]', …)
	 * @param	array		$order			ソート順（ORDER句に利用、「,」で結合）　array('[フィールド名1] => 'DESC|ASC', …)
	 * @param	bool		$lock_flg		行ロックフラグ（true:ロックする、false:ロックしない）
	 * @return	array						情報を格納した連想配列（対象がない場合は空配列）
	 */
	function simpleSelect($table, $where = array(), $order = array(), $lock_flg = false, $limit = "", $lock_share_flg = false)
	{
		if ($lock_flg) {
			// ロック情報を記録（ロック抜けの更新を見つけやすくするため）
			if (DEBUG_MODE) {
				// デバッグモードの場合のみ
				$this->lock_records_info[] = array('table' => $table, 'where' => $where);
			}
		}

		// WHERE句を生成
		$sql_param = array();
		if (0 == count($where)) {
			$sql_where_str = '';
		} else {
			$sql_where = array();
			foreach ($where as $key => $val) {
				$sql_where[] = $key . ' = ?';
				$sql_param[] = $val;
			}
			$sql_where_str = 'WHERE ' . implode(' AND ', $sql_where);
		}

			// ORDER句を生成
		if (0 == count($order)) {
			$sql_order_str = '';
		} else {
			$sql_order = array();
			foreach ($order as $key => $val) {
				$sql_order[] = $key . ' ' . $val;
			}
			$sql_order_str = 'ORDER BY ' . implode(', ', $sql_order);
		}

		//limit句を生成
		if (empty($limit)) {
			$sql_limit_str = '';
		} else {
			$sql_limit_str = ' limit ' . $limit;
		}

		// SQLを生成
		$sql_str = "SELECT * FROM $table $sql_where_str $sql_order_str $sql_limit_str";
		if ($lock_flg==true && $lock_share_flg==false) {
			$sql_str .= ' FOR UPDATE';
		} elseif ($lock_flg==false && $lock_share_flg==true) {
			$sql_str .= ' LOCK IN SHARE MODE';
		}

		$sql_str .= ";";


		// SQLを実行
//		$this->logger->log(sprintf("simpleSelect sql : (%s)", $sql_str), PEAR_LOG_DEBUG);
		$stmt = $this->db->prepare($sql_str);
		if(!empty($sql_param)){
			$stmt->execute($sql_param);
		}else{
			//全件検索
			$stmt->execute();
		}

/*
		if ($lock_flg) {
			$this->logger->log(sprintf('汎用セレクト　行ロックを設定しました　 sql=%s;, param={%s}', $sql_str, implode(', ', $sql_param)), PEAR_LOG_DEBUG);
		}
*/
		$data = array();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$data[] = $row;		// 行データを格納
		}
		return $data;
	}

	//*****
	//汎用インサート
	//汎用的に使える簡易インサート処理メソッド
	//@param string $table：テーブル名
	//@param array  $data：インサートするデータ配列　array('[フィールド名]' => '[挿入する値]', …)
	//@param bool   $update_flg：既にある場合は更新するかのフラグ
	//@param string $logging：'no_logging'を指定するとログに残らない
	//@return true：成功, false:失敗
	function simpleInsert($table, $data, $update_flg=false, $logging=null)
	{
		if ('' != $table and is_array($data)) {
			$sql_target = $sql_values = $sql_param = array();

			//data にこれらのフィールドがなければ、現在時刻で更新する。
			if (!isset($data['update_dt'])) {
				$data['update_dt'] = date('Y-m-d H:i:s', CommonFunctions::getNowTime());
			}

			//updateになる場合はcreate_dtを更新しない
			$update_data = $data;

			if(!isset($data['create_dt'])){
				$data['create_dt'] = date('Y-m-d H:i:s', CommonFunctions::getNowTime());
			}

			//データの数だけ繰り返し
			foreach ($data as $key => $val) {
				$sql_values[] = '?';
				$sql_param[]  = $val;
				$sql_target[] = $key;
			}

			// SQLを生成
			$sql_target_str = implode(', ', $sql_target);
			$sql_values_str = implode(', ', $sql_values);
			$sql_str        = "INSERT INTO $table ($sql_target_str) values ($sql_values_str)";

			if($update_flg){
				$sql_update = array();
				foreach ($update_data as $field => $value) {
					$sql_update[] = "{$field} = ?";
					$sql_param[]  = $value;
				}
				$sql_str .= ' ON DUPLICATE KEY UPDATE ';
				$sql_str .= implode(', ', $sql_update);
			}

			// SQLを実行
			$stmt = $this->db->prepare($sql_str);
			$stmt->execute($sql_param);

			// 影響のあった行が0行の場合は失敗とする
			if (0 < $stmt->rowCount()) {
				if ($logging != 'nologging') {
					$this->logger->log(
						sprintf('汎用インサート　テーブルにインサートしました sql=%s;, param={%s}, id=%s',
							$sql_str, implode(', ', $sql_param), $this->db->lastInsertId()), PEAR_LOG_DEBUG);
				}
				return true;
			}
		}

		// インサート失敗
		if ($logging != 'nologging') {
			$this->logger->log(
				sprintf('汎用インサート　テーブルへのインサートに失敗しました sql=%s;, param={%s}',
					$sql_str, implode(', ', $sql_param)), PEAR_LOG_ERR);
		}
		return false;
	}


	//*****
	//アドバイスエンジン用インサート
	//@param string $table：テーブル名
	//@param array  $data：インサートするデータ配列　array('[フィールド名]' => '[挿入する値]', …)
	//@param bool   $update_flg：既にある場合は更新するかのフラグ
	//@param string $logging：'no_logging'を指定するとログに残らない
	//@return true：成功, false:失敗
	function simpleInsertAdvice($table, $data, $update_flg=false, $logging=null)
	{
		if ('' != $table and is_array($data)) {
			$sql_target = $sql_values = $sql_param = array();

			//データの数だけ繰り返し
			foreach ($data as $key => $val) {
				$sql_values[] = '?';
				$sql_param[]  = $val;
				$sql_target[] = $key;
			}

			// SQLを生成
			$sql_target_str = implode(', ', $sql_target);
			$sql_values_str = implode(', ', $sql_values);
			$sql_str        = "INSERT INTO $table ($sql_target_str) values ($sql_values_str)";

			if($update_flg){
				$sql_update = array();
				foreach ($update_data as $field => $value) {
					$sql_update[] = "{$field} = ?";
					$sql_param[]  = $value;
				}
				$sql_str .= ' ON DUPLICATE KEY UPDATE ';
				$sql_str .= implode(', ', $sql_update);
			}

			// SQLを実行
			$stmt = $this->db->prepare($sql_str);
			$stmt->execute($sql_param);

			// 影響のあった行が0行の場合は失敗とする
			if (0 < $stmt->rowCount()) {
				if ($logging != 'nologging') {
					$this->logger->log(
						sprintf('汎用インサート　テーブルにインサートしました sql=%s;, param={%s}, id=%s',
							$sql_str, implode(', ', $sql_param), $this->db->lastInsertId()), PEAR_LOG_DEBUG);
				}
				return true;
			}
		}

		// インサート失敗
		if ($logging != 'nologging') {
			$this->logger->log(
				sprintf('汎用インサート　テーブルへのインサートに失敗しました sql=%s;, param={%s}',
					$sql_str, implode(', ', $sql_param)), PEAR_LOG_ERR);
		}
		return false;
	}

	function getUserCount($uid, $pwd)
	{
		$sql_str  = " SELECT count(*) ";
		$sql_str .= " FROM `user_mst` ";
		$sql_str .= " WHERE ";
		$sql_str .= sprintf("user_id = '%s' and password = '%s';", $uid, $pwd);
		$stmt = $this->db->prepare($sql_str);
		$stmt->execute();

		$data = array();
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$data[] = $row;		// 行データを格納
		}

		return $data;
	}

	/**
	 * 汎用アップデート
	 * 汎用的に使える簡易アップデート処理メソッド
	 * @param	string		$table			テーブル名
	 * @param	array		$data			アップデートするデータ配列　array('[フィールド名]' => '[更新する値]', …)
	 * @param	array		$where			更新条件（WHERE句に利用、ANDで結合）　array('[フィールド名]' => '[条件値]', …)
	 * @return								true:成功, false:失敗
	 */
	function simpleUpdate($table, $data, $where)
	{

		if ('' != $table and is_array($data) and 0 < count($where)) {
			$sql_set = $sql_param = $sql_where = array();

			// 更新時刻を上書き
			$data['update_dt'] = date('Y-m-d H:i:s', CommonFunctions::getNowTime());

			// 更新データの数だけ繰り返し
			foreach ($data as $key => $val) {
				$sql_set[] = $key . ' = ?';
				$sql_param[] = $val;
			}

			// WHERE句を生成
			foreach ($where as $key => $val) {
				$sql_where[] = $key . ' = ?';
				$sql_param[] = $val;
			}

			// SQLを生成
			$sql_set_str = implode(', ', $sql_set);
			$sql_where_str = implode(' AND ', $sql_where);
			$sql_str = "UPDATE $table SET $sql_set_str WHERE $sql_where_str";
			// ロックなしでアップデートしていないかのチェック
			if (DEBUG_MODE) {
				// デバッグモードの場合のみ
				$this->checkRecordsLockStatus($table, $where);
			}

			// SQLを実行
			$stmt = $this->db->prepare($sql_str);
			$stmt->execute($sql_param);

			// 影響のあった行が1行でなければ失敗とする
			if (1 == $stmt->rowCount()) {
				$this->logger->log(sprintf('汎用アップデート　テーブルの更新を行いました sql=%s;, param={%s}', $sql_str, implode(', ', $sql_param)), PEAR_LOG_DEBUG);
				return true;
			} elseif (0 == $stmt->rowCount()) {
				// 0行が返ってきた場合はSELECTしなおす
				// PDO+MySQLの仕様として変更がなかった場合更新が行われないため
				// 将来PDO::MYSQL_ATTR_FOUND_ROWS（CLIENT_FOUND_ROWSを設定するオプション）が実装されれば解決する予定
				$row = $this->simpleSelect($table, $where);
				if (0 != count($row)) {
					//$this->logger->log(sprintf('汎用アップデート　テーブルの変更なし　table=%s', $table), PEAR_LOG_DEBUG);
					return true;
				}
			}
		}

		// アップデート失敗
		$this->logger->log(sprintf('汎用アップデート　テーブルのアップデートに失敗しました sql=%s;, param={%s}', $sql_str, implode(', ', $sql_param)), PEAR_LOG_ERR);
		return false;
	}

	/**
	 * 対象がロックされているかを調べ、ロックされていないようであれば警告ログを出力する
	 * @param	string	$table	対象テーブル名
	 * @param	array	$where	検索条件
	 */
	function checkRecordsLockStatus($table, $where)
	{
		// 最後に追加されたロック情報がダミーの場合は処理スキップ
		$last_arr = end($this->lock_records_info);
		if (0 == count($last_arr)) {
			// 処理スキップ
			array_pop($this->lock_records_info);	// 最後の要素を取り除く
			return;
		}

		$find_flg = false;		// 対象発見フラグ

		// ロックの数だけ繰り返し
		foreach ($this->lock_records_info as $row) {
			if ($row['table'] == $table and $row['where'] == $where) {
				// 対象テーブルと条件が一致した場合
				$find_flg = true;
				break;
			}
		}
		if (!$find_flg) {
			// ロックが見つからなかった場合
			$this->logger->log(sprintf('データベースアクセスクラス　デバッグ情報　【警告】ロック無しで更新している table=%s, param={%s : %s}', $table, implode(', ', array_keys($where)), implode(', ', $where)), PEAR_LOG_NOTICE);
		}
	}

	function execSql($sql, $param = array())
	{
		$data = array();
		$stmt = $this->db->prepare($sql);

		if(isset($param) && !empty($param)){
			$stmt->execute($param);
		}else{
			$stmt->execute();
		}

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			//行データを格納
			$data[] = $row;
		}

		return $data;
	}

	function execSqlforUpdate($sql, $param = array())
	{
		$data = array();
		$stmt = $this->db->prepare($sql);

		if(isset($param) && !empty($param)){
			$stmt->execute($param);
		}else{
			$stmt->execute();
		}

		if ($stmt->rowCount() > 0) {
			$this->logger->log(
				sprintf(
					'汎用アップデート:テーブルの更新を行いました sql=%s;, param={%s}',
					$sql, implode(', ', $param)
				),
				PEAR_LOG_DEBUG
			);
			return true;
		} elseif ($stmt->rowCount() === 0) {
			// 0行が返ってきた場合はSELECTしなおす
			// PDO+MySQLの仕様として変更がなかった場合更新が行われないため
			// 将来PDO::MYSQL_ATTR_FOUND_ROWS（CLIENT_FOUND_ROWSを設定するオプション）が実装されれば解決する予定
			$this->logger->log(
				sprintf(
					'汎用アップデート　テーブルの変更なし　sql=%s;, param={%s}',
					$sql, implode(', ', $param)
				),
				PEAR_LOG_DEBUG
			);
			return true;
		}
		return false;
	}

	function execSqlOnly($sql, $param = array())
	{
		$stmt = $this->db->prepare($sql);

		if(isset($param) && !empty($param)){
			$stmt->execute($param);
		}else{
			$stmt->execute();
		}
	}


  function changeDb(){

	}


}
?>
