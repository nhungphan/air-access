'use strict';

$(document).ready(function(){

    (function(){

        var ACCORDION_BTNS = $(".js-qa__accordion__btn");
        var QA_QES = $(".qa__item__question");
        var QA_ANS = $(".qa__item__answer");

        ACCORDION_BTNS.on("click",function(){

            var id = ACCORDION_BTNS.index(this);

            if(ACCORDION_BTNS.eq(id).html() === "▼")
            {
                ACCORDION_BTNS.eq(id).html("▲");
            }else{
                ACCORDION_BTNS.eq(id).html("▼");
            }

            if(!QA_QES.eq(id).hasClass("close"))
            {
                QA_QES.eq(id).addClass("close");
            }else{
                QA_QES.eq(id).removeClass("close");
            }

            QA_ANS.eq(id).slideToggle(250);
        });

    }());

});