'use strict';

$(document).ready(function(){

    (function(){

        var WINDOW = $(window);
        var WRAPPER = $("#wrapper");
        var FOOTER = $(".footer")
        var BTN = $(".js-triggerBtn--top");

        var defPosi = 16;
        var btnHeight = 78;
        var showPosi = WINDOW.innerHeight();

        resize();
        WINDOW.on("scroll resize",resize);

        BTN.on("click",function () {
            $("body,html").animate({ scrollTop: 0 }, 500);
        });

        function resize()
        {

            showPosi = WINDOW.innerHeight();

            if( showPosi > WINDOW.scrollTop() )
            {
                BTN.fadeOut();                
            } else {
                BTN.fadeIn();
            }

            var posi = (WINDOW.innerHeight() + WINDOW.scrollTop()) - (btnHeight + defPosi);
            var remein = ( WRAPPER.outerHeight() - WINDOW.scrollTop() ) - WINDOW.innerHeight();
            remein = remein - FOOTER.outerHeight();

            if(remein < 0)
            {
                posi = posi + remein;
            }

            BTN.css({top: posi + "px"});

        }

    }());

});
