'use strict';

$(document).ready(function(){

    (function(){

        var SPIN_INPUTS = $(".js-spin__input");
        var SPIN_ADD_BTNS = $(".js-spin__btn--add");
        var SPIN_SUB_BTNS = $(".js-spin__btn--sub");

        //init
        SPIN_INPUTS.each(function(index){
            $(this).val(1);
        });

        SPIN_SUB_BTNS.on("click",function(){
            var index = SPIN_SUB_BTNS.index(this);
            if(isNumValue(index))
            {
                var val =  parseInt( SPIN_INPUTS.eq(index).val() );
                if( (val-1) >= 1 )
                {
                    val = val-1 
                }
                SPIN_INPUTS.eq(index).val(val);
            }
        });

        SPIN_ADD_BTNS.on("click",function(){
            var index = SPIN_ADD_BTNS.index(this);
            if(isNumValue(index))
            {
                var val =  parseInt( SPIN_INPUTS.eq(index).val() );
                if( val < 99999 )
                {
                    val = val + 1 
                }
                SPIN_INPUTS.eq(index).val(val);
            }
        });

        function isNumValue(index)
        {   
            if( SPIN_INPUTS.eq(index).val() != "" )
            {
                var reg = /^\d*$/;
                var val = SPIN_INPUTS.eq(index).val();
                return reg.test(val);
            }
            return false;
        }

    }());

});
