'use strict';

$(document).ready(function(){

    (function(){
        
        var CBOX = $(".js-isCheckBox");
        var SUBMIT_BTN = $(".js-submit__btn");

        CBOX.change(function(){
            if( CBOX.prop("checked") )
            {
                SUBMIT_BTN.removeClass("btn--disabled");
                SUBMIT_BTN.addClass("btn--orange");
            }
            else
            {
                SUBMIT_BTN.removeClass("btn--orange");
                SUBMIT_BTN.addClass("btn--disabled");
            }
        });

        SUBMIT_BTN.on("click",function(){

            if(!SUBMIT_BTN.hasClass("btn--orange"))
            {
                return false;
            }

            var email = $('#air_access_member_email').val();
            var emailRepeated = $('#air_access_member_repeated_email').val();
            if(email != emailRepeated){
                alert('返信用メールアドレスと確認用メールアドレスが一致しません');
                return false;
            }

            var paymentId = $('#paymentSelector').val();
            if(paymentId == '5'){
                if($('#cardNumber').val() == ''){
                    alert('カード番号を入力してください');
                    return false;
                }
            }

            document.form1.submit();
        });

    }());

});


