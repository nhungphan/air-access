'use strict';

var passwordFunc = (function() {

  var pass1 = $("#login_pass");
  var email1 = $("#login_email");
  var checkPassButton = $("#checkPassButton");
  var email1_error = $("#email1-error");
  var email2_error = $("#email2-error");
  var pass1_error = $("#pass1-error");
  var pass1_error2 = $("#pass1-error2");


  // フォームのフォーカス時の処理
  function onfocusPass() {
    email1
      .on("focusout", function() {
        chMail();
        var n = [email1.val()].length;
        checkEmailInput(n);
      });

    pass1
      .on("focusout", function() {
        checkPassLength();
        checkPassText();
        var n = [pass1.val()].length;
        checkPassInput(n);
      });
  }

  // メールアドレスかどうかチェックする
  function chMail() {
    var regex_mail = /.+@.+\..+/;
    var count_email_alert = 'メールアドレスを入力してください';
    if(email1.val().match(regex_mail)) {
      email1_error.text("");
    }else{
      email1_error.text(count_email_alert);
    }
  }

  // パスワードの文字数チェック
  function checkPassLength() {
    var pass_length = pass1.val().length;
    var count_pass_alert = '4文字以上50文字以下で入力してください';
    if(pass_length < 4) {
      pass1_error.text(count_pass_alert);
    } else {
      pass1_error.text("");
    }
  }

  // パスワードの文字列チェック
  function checkPassText() {
    var regex = /^[a-zA-Z0-9]+$/;
    var no_regex_alert = '半角英数字で入力してください';
      if(pass1.val().match(regex)) {
        pass1_error2.text("");
      } else {
        pass1_error2.text(no_regex_alert);
      }
  }

  // パスワードの入力チェック
  function checkPassInput(n) {
    var no_input_pass_alert = 'パスワードを入力してください';
    for(var i = 1; i <= n; i++) {
      var pass = $("#pass" + i);
      var pass_error = $("#pass" + i + "-error");
      if(pass.val() == "") {
        pass_error.text(no_input_pass_alert);
        if(i == "1"){
          pass1_error2.text("");
        }
      }
    }
  }

  // emailの入力チェック
  function checkEmailInput(n) {
    var no_input_email_alert = 'メールアドレスを入力してください';
    for(var i = 1; i <= n; i++) {
      var email = $("#email" + i);
      var email_error = $("#email" + i + "-error");
      if(email.val() == "") {
        email_error.text(no_input_email_alert);
        if(i == "1"){
          email1_error2.text("");
        }
      }
    }
  }

  // パスワード確認ボタンを押したときの処理
  function checkPass() {
    if(email1.val() == "") {
      var n = email1.val().length;
      checkEmailInput(n);
    } else {
      chMail();
    }

    if(pass1.val() == "") {
      var n = pass1.val().length;
      checkPassInput(n);
    } else {
      checkPassLength();
      checkPassText();
      if(pass1_error.text() != "" || pass1_error2.text() != "" || email1_error.text() != "") {
        return false;
      }
    }
  }

  return {
    // パブリックメソッド
    start: function() {
      onfocusPass();
      checkPassButton.on("mousedown", checkPass);
    }
  };
})();

passwordFunc.start();
