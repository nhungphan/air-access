'use strict';

$(document).ready(function(){

    (function(){

        var BTNLOWER_POSI = 32;

        var WINDOW = $(window);
        var BODY = $("body");
        var WRAPPER = $(".wrapper");
        var HEADER= $(".header")
        var FOOTER = $(".footer");
        var GMENU = $(".g-menu");
        var GMENU_BTN = $(".header__trigger");
        var BTN = $(".js-triggerBtn--top");

        var btnHeight = BTN.height();
        var bodyHeight = BODY.height();
        var footerHeight = FOOTER.height();
        var wrapperHeight = WRAPPER.height();

        var isGoTopScroll = false;

        // Action GoTOP
        BTN.on("click",function () {
            isGoTopScroll = true;
            BODY.animate({ scrollTop: 0 }, 500,function(){
                isGoTopScroll = false;
            });
        });

        //Action Global Menu
        GMENU_BTN.on("click",function(){
            
            var rightPosi = parseInt(GMENU.css("right"));
            var stateOp = 1;

            if(!GMENU.hasClass("js-open"))
            {
                GMENU.addClass("js-open");
                rightPosi = rightPosi + GMENU.width();
            }
            else
            {
                GMENU.removeClass("js-open");
                rightPosi = rightPosi - GMENU.width();
                stateOp = 0;                
            }
            GMENU.css({opacity:stateOp, right:rightPosi});
        });

        // Action ReSize        
        WINDOW.on("resize",actResize);
        actResize();
        
        // Action Scroll
        WINDOW.on("scroll",actScroll);
        actScroll();


        function actResize()
        {
            HEADER.css({width:WRAPPER.width()});

            var g_wrapper = GMENU.children(".g-menu__wrapper");
            var rightPosi =( WINDOW.width() - WRAPPER.width() ) * 0.5;
            rightPosi = rightPosi - GMENU.width();
            GMENU.removeClass("js-open");
            GMENU.css({height: g_wrapper.outerHeight(), right:rightPosi, opacity:0});
        }

        function actScroll()
        {
            var posi = WINDOW.scrollTop();

            bodyHeight = BODY.height();
            footerHeight = FOOTER.height();
            wrapperHeight = WRAPPER.height();

            var btnPosi = (bodyHeight + posi) - btnHeight;
            var diff = (wrapperHeight - footerHeight) - posi ;

            if (posi > 256 && !isGoTopScroll) {
                BTN.fadeIn();
            } else {
                BTN.fadeOut();
            }

            if( bodyHeight > diff ) {
                btnPosi = btnPosi - (bodyHeight - diff);
            }

            BTN.css({top: (btnPosi - BTNLOWER_POSI) + "px"});

            actResize();
        }

    }());

});
