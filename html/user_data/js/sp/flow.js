'use strict';

$(document).ready(function(){

    $(".step__accordion--ana").on("click",function(){
        actAccordion(this);
    });

    $(".step__accordion--jal").on("click",function(){
        actAccordion(this);
    });

    function actAccordion(self)
    {
        var btn = $(self).children(".step__accordion__title")
        var target = $(self).children(".step__accordion__content");
        if(!btn.hasClass("open"))
        {
            btn.addClass("open");
        }else{
            btn.removeClass("open");
        }
        target.slideToggle();
    }

});
