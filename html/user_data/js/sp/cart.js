'use strict';

$(document).ready(function(){

    (function(){

        var SPIN_INPUTS = $(".js-spin__input");
        var SPIN_ADD_BTNS = $(".js-spin__btn--add");
        var SPIN_SUB_BTNS = $(".js-spin__btn--sub");

        SPIN_INPUTS.keypress(function(e){
            if(e.which == 13){
                var reg = /^\d*$/;
                var q = parseInt($('#' + e.target.id).val());

                //-- 数値文字以外は１にする
                if(!reg.test(q) || q < 1){
                    $('#' + e.target.id).val(1);
                }

                changeQuantity(e.target.id.match(/[0-9]+/));

                return false;
            }
        });

        SPIN_SUB_BTNS.on("click",function(){
            var index = SPIN_SUB_BTNS.index(this);
            if(isNumValue(index))
            {
                var val =  parseInt( SPIN_INPUTS.eq(index).val() );
                if( (val-1) >= 1 )
                {
                    val = val-1 
                }
                SPIN_INPUTS.eq(index).val(val);

                changeQuantity(SPIN_INPUTS.eq(index).attr("id").match(/[0-9]+/));
            }
        });

        SPIN_ADD_BTNS.on("click",function(){
            var index = SPIN_ADD_BTNS.index(this);
            if(isNumValue(index))
            {
                var val =  parseInt( SPIN_INPUTS.eq(index).val() );
                if( val < 99999 )
                {
                    val = val + 1 
                }
                SPIN_INPUTS.eq(index).val(val);

                changeQuantity(SPIN_INPUTS.eq(index).attr("id").match(/[0-9]+/));
            }
        });

        function isNumValue(index)
        {   
            if( SPIN_INPUTS.eq(index).val() != "" )
            {
                var reg = /^\d*$/;
                var val = SPIN_INPUTS.eq(index).val();
                return reg.test(val);
            }
            return false;
        }

    }());

});
