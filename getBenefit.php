#!/usr/bin/php -q
<?php
  require_once ('/var/www/air-access/lib/DatabaseAccessMysql.php');

  $item = $argv[1]; // var_dump($item);
  $data = json_decode($item, true); // var_dump($data);

  //-- パラメータ取得
  $quantity = intval($data["quantity"]);

  $dbam = DatabaseAccessMysql::singleton("mysql");
  $dbam->connectToDb();

//  $res = $dbam->simpleSelect(
//    'dtb_benefit',                              //-- table
//    array("order_id" => null, "del_flg" => 0),  //-- where : sample : array('id' => '10')$where,
//    array(),                                    //-- order
//    false,                                      //-- lock
//    $quantity                                   //-- limit
//  );

  $res = $dbam->execSql(
    'select * from `dtb_benefit` where `order_id` is null and `del_flg` <> 1 limit ' . $quantity,
    array()
  );

  exit(json_encode($res));
?>
