<?php

/*
 * This file is part of the RecommendInCart
 *
 * Copyright (C) 2017 受注対応状況一括更新 for EC-CUBE3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\OrderStatusAll;

use Eccube\Application;
use Eccube\Event\EventArgs;
use Eccube\Event\TemplateEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class OrderStatusAllEvent
{

    /** @var  \Eccube\Application $app */
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        
    }

    public function onRenderAdminOrderIndex(TemplateEvent $event)
    {
        $search = '<li><a href="{{ url(\'admin_order_mail_all\') }}">メール一括通知</a></li>';
        $tag = <<< EOT
<li><a href="{{ url('admin_plugin_OrderStatus_all') }}">対応状況一括更新</a></li>
EOT;
        $source = str_replace($search, $search . $tag, $event->getSource());
        $event->setSource($source);
    }

}
