<?php

/*
 * This file is part of the OrderStatusAll
 *
 * Copyright (C) 2017 受注対応状況一括更新 for EC-CUBE3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\OrderStatusAll\Controller;

use Eccube\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderStatusAllController
{

    /**
     * OrderStatusAll画面
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {
        $builder = $app['form.factory']->createBuilder('order_status_all');
        $form = $builder->getForm();

        if ('POST' === $request->getMethod()) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $dateTime = new \DateTime();

                $data = $form->getData();

                $orders = explode(',', $request->get('orders'));

                foreach ($orders as $order_id) {
                    $Order = $app['eccube.repository.order']->find($order_id);

                    if (is_null($Order)) {
                        throw new NotFoundHttpException('order not found.');
                    }

                    // 発送済
                    if ($data["order_status"]->getId() == $app['config']['order_deliv']) {
                        // 編集前と異なる場合のみ更新
                        if ($Order->getOrderStatus()->getId() != $data["order_status"]->getId()) {
                            $Order->setCommitDate($dateTime);
                            // お届け先情報の発送日も更新する.
                            $Shippings = $Order->getShippings();
                            foreach ($Shippings as $Shipping) {
                                $Shipping->setShippingCommitDate($dateTime);
                            }
                        }
                        // 入金済
                    } elseif ($data["order_status"]->getId() == $app['config']['order_pre_end']) {
                        // 編集前と異なる場合のみ更新
                        if ($Order->getOrderStatus()->getId() != $data["order_status"]->getId()) {
                            $Order->setPaymentDate($dateTime);
                        }
                    }

                    $Order->setOrderStatus($data["order_status"]);
                }

                $app['orm.em']->flush();

                foreach ($orders as $order_id) {
                    $Order = $app['eccube.repository.order']->find($order_id);

                    $Customer = $Order->getCustomer();

                    if ($Customer) {
                        // 会員の場合、購入回数、購入金額などを更新
                        $app['eccube.repository.customer']->updateBuyData($app, $Customer, $data["order_status"]->getId());
                    }
                }

                return $app->redirect($app->url('admin_order'));
            } else {
                $app->addError('admin.register.failed', 'admin');
            }
        }

        $filter = function ($v) {
            return preg_match('/^ids\d+$/', $v);
        };
        $map = function ($v) {
            return preg_replace('/[^\d+]/', '', $v);
        };
        $keys = array_keys($request->query->all());
        $idArray = array_map($map, array_filter($keys, $filter));
        $orders = implode(',', $idArray);

        return $app->render('OrderStatusAll/Resource/template/admin/order_status_all.twig', array(
                    'form' => $form->createView(),
                    'orders' => $orders,
        ));
    }

}
