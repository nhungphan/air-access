<?php

namespace Plugin\OrderStatusAll\Tests\Web\Admin;

use Eccube\Tests\Web\Admin\AbstractAdminWebTestCase;

/**
 * Description of OrderStatusAll Test
 *
 * @author kurozumi
 */
class OrderStatusAllTest extends AbstractAdminWebTestCase
{
    public function testRouting()
    {
        $this->client->request('GET', $this->app->url('admin_plugin_OrderStatus_all'));
        $this->assertTrue($this->client->getResponse()->isSuccessful());

    }

}
