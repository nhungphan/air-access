#!/usr/bin/php -q
<?php
  require_once ('/var/www/air-access/lib/DatabaseAccessMysql.php');

  $item = $argv[1]; // var_dump($item);
  $data = json_decode($item, true); // var_dump($data);

  //-- パラメータ取得
  $orderId = $data["orderId"];

  $dbam = DatabaseAccessMysql::singleton("mysql");
  $dbam->connectToDb();

  //-- トランザクション開始
  $dbam->execSql('begin', array());

  //-- 優待番号を発送したオーダーの `commit_date` と `status` を更新
  //-- `status` = 5 : 発送済み
  try{
    $res = $dbam->execSql(
      'update `dtb_order` set `commit_date` = CURRENT_TIMESTAMP(), `status` = 5 where `order_id` = :orderId and `del_flg` <> 1',
      array(':orderId' => $orderId)
    );
    $dbam->execSql('commit', array());
  }
  catch(Exception $ex){
    $dbam->execSql('rollback', array());
    $res = null;
  }
  
  exit(json_encode($res));
?>
