#!/usr/bin/php -q
<?php
require_once ('/var/www/air-access/lib/DatabaseAccessMysql.php');


$item = $argv[1];

$data = json_decode($item, true);


switch ($data["proc"]){
    case 'ins':
      $res = self::insert($data)
      break;

    case 'up':
      $res = self::update($data)
      break;

    case 'select':
      $ret = self::select($data)
      $res = json_encode($ret);
      break;

  default:
    );


exit($res);



// tblにupdate
//* @param	array		$data["table"]=>テーブル名
//* @param	array		$data["set"]["フィールド名"]=> '[更新する値]'
//* @param	array		$data["where"][フィールド名]' => '[条件値]'
//* @return								true:成功, false:失敗
function update($data){

  $res = "";
  $table = "";
  $set = array();
  $where = array();

  $table = $data["table"];

  foreach($data["set"] as $k => $v){
    $set[$k] = $v;
  }

  foreach($data["where"] as $ky => $vl){
    $where[$ky] = $vl;
  }

  $dba = DatabaseAccessMysql::singleton("mysql");
  $dba->connectToDb();
  $res = $dba->simpleUpdate($table, $set, $where);

  return $res;
}


// tblにinsert
//* @param	array		$data["table"]=>テーブル名
//* @param	array		$data["put"][フィールド名]' => '[挿入する値]'
//* @return								true:成功, false:失敗
//@return true：成功, false:失敗

function insert($data){

  $res = "";
  $table = "";
  $put = array();

  $table = $data["table"];

  foreach($data["put"] as $k => $v){
    $set[$k] = $v;
  }

  $dba = DatabaseAccessMysql::singleton("mysql");
  $dba->connectToDb();
  $res = $dba->simpleInsert($table, $put);

  return $res;
}


// tblにselect
//* @param	array		$data["table"]=>テーブル名
//* @param	array		$data["where"][フィールド名]' => '[条件値]'   （WHERE句に利用、ANDで結合）
//* @param	array		$data["order"][フィールド名]' => '['DESC|ASC']'   ソート順（ORDER句に利用、「,」で結合）
//* @param	array		$data["lock_flg"] => ['true\false']  行ロックフラグ（true:ロックする、false:ロックしない）
//* @param	array		$data["limit"] => ['int']
//* @param	array		$data["lock_share_flg"] => ['true\false']
//@return array						情報を格納した連想配列（対象がない場合は空配列）
function select($data){

  $res = "";
  $table = "";
  $where = array();
  $order = array();
  $lock_flg = false;
  $lock_share_flg = false;
  $limit = "";

  $table = $data["table"];

  foreach($data["where"] as $k => $v){
    $where[$k] = $v;
  }

  foreach($data["order"] as $k => $v){
    $sort[$k] = $v;
  }

  if (isset($data['lock_flg'])){
      $lock_flg = $data["lock_flg"];
  }
  if (isset($data['limit'])){
      $lock_flg = $data["limit"];
  }
  if (isset($data['$lock_share_flg'])){
      $lock_flg = $data["$lock_share_flg"];
  }

  $dba = DatabaseAccessMysql::singleton("mysql");
  $dba->connectToDb();
  $res = $dba->simpleInsert($table, $where = array(), $order = array(), $lock_flg, $limit, $lock_share_flg);

  return $res;
}




/*


  // dtb_benefit_tblにinsert
  //@param string $table：テーブル名
	//@param array  $data：インサートするデータ配列　array('[フィールド名]' => '[挿入する値]', …)
	//@return true：成功, false:失敗
  function insertBenefit($data){
    $where = json_decode($data, true);

    $dba = DatabaseAccessMysql::singleton("mysql");
    $dba->connectToDb();
    $res = $dba->simpleInsert("dtb_benefit", $where);

    $data = json_encode($res);

    return $data;
  }


  // dtb_benefit_tblにupdate
  //* @param	string		$table			テーブル名
  //* @param	array		$data			アップデートするデータ配列　array('[フィールド名]' => '[更新する値]', …)
  //* @param	array		$where			更新条件（WHERE句に利用、ANDで結合）　array('[フィールド名]' => '[条件値]', …)
  //* @return								true:成功, false:失敗
  function updateBenefit($where){
    $dba = DatabaseAccessMysql::singleton("mysql");
    $dba->connectToDb();
    $res = $dba->simpleUpdate("dtb_benefit", $data, $where);

    $data = json_encode($res);

    return $data;
  }



  // 株主優待テーブルの情報を取得
  //* @param	string		$table			テーブル名
  //* @param	array		$where			更新条件（WHERE句に利用、ANDで結合）　array('[フィールド名]' => '[条件値]', …)
  //* @param	array		$order			ソート順（ORDER句に利用、「,」で結合）　array('[フィールド名1] => 'DESC|ASC', …)
  //* @param	bool		$lock_flg		行ロックフラグ（true:ロックする、false:ロックしない）
  //* @return	array						情報を格納した連想配列（対象がない場合は空配列）
  function getBenefitData($where){
    $dba = DatabaseAccessMysql::singleton("mysql");
    $dba->connectToDb();
    $res = $dba->simpleSelect('dtb_benefit', $where);

    $data = json_encode($res);

    return $data;
  }

  // tblにupdate
  //* @param	array		$data["table"]=>テーブル名
  //* @param	array		$data["set"]["フィールド名"]=> '[更新する値]'
  //* @param	array		$data["where"][フィールド名]' => '[条件値]'
  //* @return								true:成功, false:失敗
  function foreachUpdate($data){
    $selecet = "";
    $ret = true;

    $dba = DatabaseAccessMysql::singleton("mysql");
    $dba->connectToDb();

    $dba->myBeginTransaction();

    // $data = $dba->SimpleSelect("dtb_benefit", $where = array("order_id" => NULL, "product_code" => $product_code, "del_flg" => "0", ), $order = array("benefit_id" => "asc"), $lock_flg = true, $limit = $num);
    $data = $dba->SimpleSelect("dtb_benefit", $where = array("order_id" => NULL, "product_code" => $product_code, "del_flg" => "0", ), $order = array("benefit_id" => "asc"), $lock_flg = true, $limit = $num);

    foreach($data as $k => $v){
      $ret = $dba->simpleUpdate("dtb_benefit", array("order_id" => $order_id), array("benefit_id" => $v["benefit_id"]));
        if(!$ret){
          $dba->myRollback();
          return $ret;
        }
    }

    $dba->myCommit();

    return $ret;
  }
*/


?>
