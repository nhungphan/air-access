DROP TABLE IF EXISTS `dtb_benefit`;
CREATE TABLE `dtb_benefit` (
  `benefit_id` int(11) NOT NULL AUTO_INCREMENT comment '株主優待ID',
  `product_code` varchar(32) NOT NULL  comment '商品コード',
  `benefit_code` varchar(16) NOT NULL comment '株主優待番号',
  `password` varchar(32) comment 'パスワード',
  `exdate_from` datetime NOT NULL  comment '有効期間 from',
  `exdate_to` datetime NOT NULL comment '有効期間 to',
  `order_id` int(11) comment '受注コード',
  `done_date` datetime comment '完了日付',
  `update_date` datetime NOT NULL comment '更新日付',
  `create_date` datetime NOT NULL comment '作成日付',
  `del_flg` smallint(6) NOT NULL DEFAULT '0' comment '削除フラグ',
  PRIMARY KEY (`benefit_id`),
  index(benefit_id, order_id)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
