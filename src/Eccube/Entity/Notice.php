<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Util\EntityUtil;

/**
 * Notice
 */
class Notice extends \Eccube\Entity\AbstractEntity
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    public function getMainListImage() {
        $NoticeImages = $this->getNoticeImage();
        return empty($NoticeImages) ? null : $NoticeImages[0];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $rank;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $url;

    /**
     * @var integer
     */
    private $select;

    /**
     * @var integer
     */
    private $link_method;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var integer
     */
    private $del_flg;

    /**
     * @var \Eccube\Entity\Member
     */
    private $Creator;

    /**
     * Constructor
     */
     public function __construct()
     {
         $this->NoticeImage = new ArrayCollection();
     }
 
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param  \DateTime $date
     * @return Notice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set rank
     *
     * @param  integer $rank
     * @return Notice
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Notice
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment
     *
     * @param  string $comment
     * @return Notice
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set url
     *
     * @param  string $url
     * @return Notice
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set select
     *
     * @param  integer $select
     * @return Notice
     */
    public function setSelect($select)
    {
        $this->select = $select;

        return $this;
    }

    /**
     * Get select
     *
     * @return integer
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * Set link_method
     *
     * @param  integer $linkMethod
     * @return Notice
     */
    public function setLinkMethod($linkMethod)
    {
        $this->link_method = $linkMethod;

        return $this;
    }

    /*
     * Get link_method
     *
     * @return integer
     */
    public function getLinkMethod()
    {
        return $this->link_method;
    }

    /**
     * Set create_date
     *
     * @param  \DateTime $createDate
     * @return Notice
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param  \DateTime $updateDate
     * @return Notice
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set del_flg
     *
     * @param  integer $delFlg
     * @return Notice
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Set Creator
     *
     * @param  \Eccube\Entity\Member $creator
     * @return Notice
     */
    public function setCreator(\Eccube\Entity\Member $creator)
    {
        $this->Creator = $creator;

        return $this;
    }

    /**
     * Get Creator
     *
     * @return \Eccube\Entity\Member
     */
    public function getCreator()
    {
        if (EntityUtil::isEmpty($this->Creator)) {
            return null;
        }
        return $this->Creator;
    }





    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $NoticeImage;

    /**
     * Add NoticeImage
     *
     * @param \Eccube\Entity\NoticeImage $noticeImage
     * @return Notice
     */
    public function addNoticeImage(\Eccube\Entity\NoticeImage $noticeImage)
    {
        $this->NoticeImage[] = $noticeImage;
 
        return $this;
    }
 
    /**
     * Remove NoticeImage
     *
     * @param \Eccube\Entity\NoticeImage $noticeImage
     */
    public function removeNoticeImage(\Eccube\Entity\NoticeImage $noticeImage)
    {
        $this->NoticeImage->removeElement($noticeImage);
    }
 
    /**
     * Get NoticeImage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoticeImage()
    {
        return $this->NoticeImage;
    }

    public function getMainFileName()
    {
        if (count($this->NoticeImage) > 0) {
            return $this->NoticeImage[0];
        } else {
            return null;
        }
    }
}
