<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Entity;

use Eccube\Common\Constant;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Benefit
 */
class Benefit extends \Eccube\Entity\AbstractEntity
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $search_word;

    /**
     * @var string
     */
    private $product_code;

    /**
     * @var string
     */
    private $benefit_code;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime
     */
    private $exdate_from;

    /**
     * @var \DateTime
     */
    private $exdate_to;

    /**
     * @var integer
     */
    private $order_id;

    /**
     * @var \DateTime
     */
    private $done_date;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var integer
     */
    private $del_flg;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product_code
     *
     * @param string $productCode
     * @return Benefit
     */
    public function setProductCode($productCode)
    {
        $this->product_code = $productCode;

        return $this;
    }

    /**
     * Get product_code
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->product_code;
    }

    /**
     * Set benefit_code
     *
     * @param string $benefitCode
     * @return Benefit
     */
    public function setBenefitCode($benefitCode)
    {
        $this->benefit_code = $benefitCode;

        return $this;
    }

    /**
     * Get benefit_code
     *
     * @return string
     */
    public function getBenefitCode()
    {
        return $this->benefit_code;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Benefit
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set exdateFrom
     *
     * @param \DateTime  $exdateFrom
     * @return Benefit
     */
    public function setExdateFrom($exdateFrom)
    {
        $this->exdate_from = $exdateFrom;

        return $this;
    }

    /**
     * Get exdateFrom
     *
     * @return \DateTime 
     */
    public function getExdateFrom()
    {
        return $this->exdate_from;
    }

    /**
     * Set exdateTo
     *
     * @param \DateTime $exdateTo
     * @return Benefit
     */
    public function setExdateTo($exdateTo)
    {
        $this->exdate_to = $exdateTo;

        return $this;
    }

    /**
     * Get exdateTo
     *
     * @return \DateTime 
     */
    public function getExdateTo()
    {
        return $this->exdate_to;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return Benefit
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set doneDate
     *
     * @param \DateTime $doneDate
     * @return Benefit
     */
    public function setDoneDate($doneDate)
    {
        $this->done_date = $doneDate;

        return $this;
    }

    /**
     * Get doneDate
     *
     * @return \DateTime 
     */
    public function getDoneDate()
    {
        return $this->done_date;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Benefit
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param  \DateTime $updateDate
     * @return Benefit
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set del_flg
     *
     * @param  integer $delFlg
     * @return Benefit
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Set search_word
     *
     * @param string $searchWord
     * @return Benefit
     */
    public function setSearchWord($searchWord)
    {
        $this->search_word = $searchWord;

        return $this;
    }

    /**
     * Get search_word
     *
     * @return string
     */
    public function getSearchWord()
    {
        return $this->search_word;
    }
}
