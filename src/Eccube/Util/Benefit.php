<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Util;


class Benefit
{

	/**
	 * データの形を整える
	 */
	public function toShape($data)
	{
		$json_data = "";
		$json_data = json_encode($data);
		$ret = self::setInsertBenefit($json_data);

		$res = json_decode($ret);

		return $res[0];
	}

	/**
	 * 入力値チェック
	 */
	public function checkData($data)
	{
		$mes = "";
		foreach($data as $key => $val){
			switch($key){
				case "product_code":
					if(empty($val)){
						$mes .= "<商品コード>";
					}
					break;

				case "benefit_code":
					if(empty($val)){
						$mes .= "<優待番号>";
					}
					break;

				case "exdate_from":
					if(empty($val)){
						$mes .= "<期限from>";
					}
					break;

				case "exdate_to":
					if(empty($val)){
						$mes .= "<期限to>";
					}
					break;
			}
		}

		if(!empty($mes)){
			$mes .= "の値が未入力です";
		}


		return $mes;
	}


	/**
	 * insert
	 * 新規登録
	 */
	public function setInsertBenefit($data)
	{
		$ret = exec("php /var/www/air-access/benefit_operation.php '{$data}' ", $reslut, $ret);

		$res = json_decode($ret);

		return $res[0];
	}

	/**
	 * update
	 * アップデート
	 */
	 public function setUpdateBenefit($data)
 	{
 		$ret = exec("php /var/www/air-access/benefit_operation.php '{$data}'", $reslut, $ret);

 		$res = json_decode($ret);

 		return $res[0];
 	}

	/**
	 * dtb_benefit
	 * セレクト
	 */
	public function getBenefitData($data)
	{
		$data = exec("php /var/www/air-access/benefit_operation.php '{$data}'  ", $reslut, $ret);

		if(isset($data) && !empty($data)){
			$res = json_decode($data);
			if(count($res) > 0){
				return $res[0];
			}
		}

		return null;
	}



}
