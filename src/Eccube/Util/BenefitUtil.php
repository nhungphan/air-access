<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
namespace Eccube\Util;

use Eccube\Entity\MailHistory;

class BenefitUtil
{
	/**
	 * dtb_benefit
	 * 優待番号情報取得
	 */
	public function GetBenefit($quantity)
	{
		$send_dt = array(
			"quantity" => $quantity
		);

		$json = json_encode($send_dt);	//dump($json);

		$data = exec("php /var/www/air-access/getBenefit.php '{$json}'", $result, $ret);
//dump($result);
//dump($ret);
//dump($data);
		return json_decode($result[1], true);
	}

	/**
	 * dtb_benefit
	 * 更新の為の優待番号情報取得
	 */
	public function GetBenefitForUpdate($orderId, $quantity)
	{
		$send_dt = array(
			"orderId" => $orderId,
			"quantity" => $quantity
		);
 
		$json = json_encode($send_dt);	//dump($json);
 
		$data = exec("php /var/www/air-access/getBenefitForUpdate.php '{$json}'", $result, $ret);
//dump($result);
//dump($ret);
//dump($data);
		 return json_decode($result[1], true);
	}

	/**
	 * dtb_benefit
	 * 更新の為の優待番号情報取得（New！）
	 */
	public function GetBenefitForUpdateNew($orderId, $productCode, $quantity)
	{
		$send_dt = array(
			"orderId" => $orderId,
			"productCode" => $productCode,
			"quantity" => $quantity
		);
 
		$json = json_encode($send_dt);	//dump($json);
 
		$data = exec("php /var/www/air-access/getBenefitForUpdateNew.php '{$json}'", $result, $ret);
//dump($result);
//dump($ret);
//dump($data);
		 return json_decode($result[1], true);
	}

	/**
	 * dtb_benefit
	 * 更新の為の優待番号情報取得
	 */
	public function DeliveredBenefit($orderId)
	{
		$send_dt = array(
			"orderId" => $orderId,
		);
  
		$json = json_encode($send_dt);	//dump($json);
  
		$data = exec("php /var/www/air-access/deliveredBenefit.php '{$json}'", $result, $ret);
 //dump($result);
 //dump($ret);
 //dump($data);
		return json_decode($result[1], true);
	}

	/**
	 * dtb_benefit
	 * 指定 order_id の優待番号の解放
	 */
	 public function RevertBenefit($orderId)
	 {
		 $send_dt = array(
			 "orderId" => $orderId,
		 );
   
		 $json = json_encode($send_dt);	//dump($json);
   
		 $data = exec("php /var/www/air-access/revertBenefit.php '{$json}'", $result, $ret);
  //dump($result);
  //dump($ret);
  //dump($data);
		 return json_decode($result[1], true);
	 }
 
    //--------------------------------------------------------------------------------
    //-- メール送信
    //--------------------------------------------------------------------------------
    public function SendMail($app, $benefits, $Order)
    {
//dump($benefits);
        $benefitCodes = array();
        $count = count($benefits);
        for($i=0; $i<$count; $i++){
//dump($benefits[$i]['benefit_code']);
            $benefitCodes[$i] = $benefits[$i]['benefit_code'];
        }
//dump($benefitCodes);

        // メール送信
        $message = $app['eccube.service.mail']->sendBenefitMailAirAccess($Order, $benefitCodes);

        // 送信履歴を保存.
        $MailTemplate = $app['eccube.repository.mail_template']->find(7);   //-- 優待番号送信メールテンプレートは '7'

        $MailHistory = new MailHistory();
        $MailHistory
            ->setSubject($message->getSubject())
            ->setMailBody($message->getBody())
            ->setMailTemplate($MailTemplate)
            ->setSendDate(new \DateTime())
            ->setOrder($Order);

        $app['orm.em']->persist($MailHistory);
        $app['orm.em']->flush($MailHistory);

        return $MailHistory;
    }

    //--------------------------------------------------------------------------------
    //-- メール送信（New！）
    //--------------------------------------------------------------------------------
    public function SendMailNew($app, $benefits, $Order)
    {
//dump($benefits);
		$benefitCodes = array();
		foreach($benefits as $key => $benefit){
			$benefitCodes[$key] = array();
			$count = count($benefit);
			for($i=0; $i<$count; $i++){
//dump($benefit[$i]['benefit_code']);
				$benefitCodes[$key][$i] = $benefit[$i]['benefit_code'] . ' / ' . $benefit[$i]['password'];
			}
		}
//dump($benefitCodes);

        // メール送信
        $message = $app['eccube.service.mail']->sendBenefitMailAirAccess($Order, $benefitCodes);

        // 送信履歴を保存.
        $MailTemplate = $app['eccube.repository.mail_template']->find(7);   //-- 優待番号送信メールテンプレートは '7'

        $MailHistory = new MailHistory();
        $MailHistory
            ->setSubject($message->getSubject())
            ->setMailBody($message->getBody())
            ->setMailTemplate($MailTemplate)
            ->setSendDate(new \DateTime())
            ->setOrder($Order);

        $app['orm.em']->persist($MailHistory);
        $app['orm.em']->flush($MailHistory);

        return $MailHistory;
    }

    //--------------------------------------------------------------------------------
    //-- 空きなしメール送信（優待番号が取得できなかった）
    //--------------------------------------------------------------------------------
    public function SendEmptyMail($app, $Order)
    {
        // エラーメール送信
        $message = $app['eccube.service.mail']->sendBenefitEmptyMailAirAccess($Order);

        // 送信履歴を保存.
        $MailTemplate = $app['eccube.repository.mail_template']->find(8);   //-- 優待番号空きなし送信メールテンプレートは '8'

        $MailHistory = new MailHistory();
        $MailHistory
            ->setSubject($message->getSubject())
            ->setMailBody($message->getBody())
            ->setMailTemplate($MailTemplate)
            ->setSendDate(new \DateTime())
            ->setOrder($Order);

        $app['orm.em']->persist($MailHistory);
        $app['orm.em']->flush($MailHistory);

        return $MailHistory;
    }
}
