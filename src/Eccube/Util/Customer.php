<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
namespace Eccube\Util;

class Customer
{

	public $json;

	/**
	 * dtb_customer
	 * カスタマー情報取得
	 */
	public function getCustomer($email)
	{
		$send_dt = array(
			"proc" => "email",
			"email" => $email
		);

		$json = json_encode($send_dt);

// echo $json;

		$data = exec("php /var/www/air-access/getcustomer.php '{$json}'", $reslut, $ret);

 // var_dump($data);

		if(isset($data) && !empty($data)){
			$res = json_decode($data);
			if(count($res) > 0){
				return $res[0];
			}
		}

		return null;
	}

	/**
	 * dtb_customer
	 * ログインチェック
	 */
	public function checkCustomer($email, $password)
	{
		$customer = self::getCustomer($email);

		// パスワードチェック
		if(count($customer)){
			if(!self::checkPassword($password, $customer->password, $customer->salt)){
				return null;
			}
		}

		return $customer;
	}

	/**
	 * パスワードのハッシュ化
	 *
	 * @param string $str 暗号化したい文言
	 * @param string $salt salt
	 * @return string ハッシュ暗号化された文字列
	 */
	function getHashString($str, $auth_magic, $salt) {
			$res = '';
			if ($salt == '') {
					$salt = $auth_magic;
			}
			$res = hash_hmac("sha256", $str . ":" . $auth_magic, $salt);

			return $res;
	}

	/**
	 * dtb_customer
	 * パスワードチェック
	 */
	public function checkPassword($password, $hashpass, $salt)
	{
		$auth_magic = "31eafcbd7a81d7b401a7fdc12bba047c02d1fae6";
    $res = false;
    if(empty($salt)){
        // 旧バージョン(2.11未満)からの移行を考慮
        $hash = sha1($password . ":" . $auth_magic);
    }else{
			$hash = self::getHashString($password, $auth_magic, $salt);
    }
    if($hash === $hashpass){
        $res = true;
    }
// var_dump($res);
    return $res;
	}

}
