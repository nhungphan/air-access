<?php

namespace Eccube\Util;

/**
 * GMO PaymentGatewayクラス.
 */
class GmoPg 
{
    //################################################################################
    //-- GMO PaymentGateway 決済
    //################################################################################
    public function ExecGMOPG($Order, $request, $token, $deviceCategory)
    {
//dump($token);
        $entryResults = array();
        $entryResult = self::_GMOPG_EntryTran($Order);
        parse_str($entryResult, $entryResults); //dump($entryResults);

//dump('_GMOPG_EntryTran');
//dump($entryResults);exit;
        if(self::_isErrorGMOPG($entryResults)){
            throw new \Exception($entryResults['ErrInfo']); //-- エラーが返って来たらExceptionをThrowする
        }
        else{
            $execResults = array();

            if($entryResults['AccessID'] != '' && $entryResults['AccessPass'] != ''){
/*****
                $execResult = $this->_GMOPG_ExecTran(
                    $Order,
                    $entryResults['AccessID'],
                    $entryResults['AccessPass'],
                    $token
                );
*****/
                $execResult = self::_GMOPG_ExecTran_3D(
                    $Order,
                    $entryResults['AccessID'],
                    $entryResults['AccessPass'],
                    $token,
                    $request->headers->get('Accept'),
                    $request->headers->get('User-Agent'),
                    //$this->isPC($request->headers->get('User-Agent')) ? 0 : 1
                    $deviceCategory
                );

                parse_str($execResult, $execResults);   //dump($execResults);
                
                if(self::_isErrorGMOPG($execResults)){
                    throw new \Exception($execResults['ErrInfo']);  //-- エラーが返って来たらExceptionをThrowする
                }
                else if(array_key_exists('PaReq', $execResults)) {  //dump('_GMOPG_ExecTran_3D');
//dump($execResult);
                    $execResults['PaReq'] = str_replace(' ', '+', $execResults['PaReq']);
//dump($execResults);//exit;
                }
            }

            return $execResults;
        }
    }

    //################################################################################
    //-- GMO PaymentGateway 認証後決済実行（本人認証サービスを使用）
    //-- 030_プロトコルタイプ（カード決済インターフェイス仕様）_1.20.pdf :: 2.2.2.4
    //################################################################################
    public function GMOPG_ExecTran_3D_AfterAuth($PaRes, $MD)
    {
        $execResults = array();
        $url = "https://pt01.mul-pay.jp/payment/SecureTran.idPass";
        $execResult = self::_postGMOPG($url, array(
            "VerSion" => "",                        //-- 1
            "PaRes" => $PaRes,                      //-- 2：本人認証サービス結果
            "MD" => $MD,                            //-- 3：取引ID
        ));

        parse_str($execResult, $execResults);   //dump($execResults);

        if(self::_isErrorGMOPG($execResults)){
            throw new \Exception($execResults['ErrInfo']);  //-- エラーが返って来たらExceptionをThrowする
        }

        return $execResults;
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway 取引登録
    //-- 030_プロトコルタイプ（カード決済インターフェイス仕様）_1.20.pdf :: 2.1.2.1
    //--------------------------------------------------------------------------------
    private function _GMOPG_EntryTran($Order)
    {
        //$url = "https://p01.mul-pay.jp/payment/EntryTran.idPass";　//本番環境
        $url = "https://pt01.mul-pay.jp/payment/EntryTran.idPass"; //テスト環境

        return self::_postGMOPG($url, array(
			"VerSion" => "",                        //-- 1
			"ShopID" => "tshop00051412",            //-- 2
			"ShopPass" => "p3q861bg",               //-- 3
			"OrderID" => $Order['id'],              //-- 4
			"JobCd" => "CAPTURE",                   //-- 5：即時売上
			"ItemCode" => "",                       //-- 6
			"Amount" => $Order['payment_total'],    //-- 7
			"Tax" => "0",                           //-- 8
            "TdFlag" => "1",                        //-- 9：本人認証サービス利用フラグ（3Dセキュア）
			"TdTenantName" => base64_encode(mb_convert_encoding("Air-Access", "EUC-JP", "auto"))          //-- 10
        ));
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway 決済実行
    //-- 030_プロトコルタイプ（カード決済インターフェイス仕様）_1.20.pdf :: 2.1.2.2
    //--------------------------------------------------------------------------------
    private function _GMOPG_ExecTran($Order, $accessId, $accessPw, $token)
    {
        $url = "https://pt01.mul-pay.jp/payment/ExecTran.idPass";
        return self::_postGMOPG($url, array(
            "VerSion" => "",                        //-- 1
            "AccessID" => $accessId,                //-- 2
            "AccessPass" => $accessPw,              //-- 3
            "OrderID" => $Order['id'],              //-- 4
            "Method" => "1",                        //-- 5：一括
            "PayTimes" => "1",                      //-- 6：1回
            "CardNo" => "",                         //-- 7
            "Expire" => "",                         //-- 8
            "SecurityCode" => "",                   //-- 9
            "Token" => $token,                      //-- 10
            "PIN" => "",                            //-- 11
            "ClientField1" => "",                   //-- 12
            "ClientField2" => "",                   //-- 13
            "ClientField3" => "",                   //-- 14
            "ClientFieldFlag" => "0"                //-- 15
        ));
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway 決済実行（本人認証サービスを使用）
    //-- 030_プロトコルタイプ（カード決済インターフェイス仕様）_1.20.pdf :: 2.2.2.2
    //--------------------------------------------------------------------------------
    private function _GMOPG_ExecTran_3D($Order, $accessId, $accessPw, $token, $httpAccept, $userAgent, $deviceCategory)
    {
        $url = "https://pt01.mul-pay.jp/payment/ExecTran.idPass";
        return self::_postGMOPG($url, array(
            "VerSion" => "",                        //-- 1
            "AccessID" => $accessId,                //-- 2
            "AccessPass" => $accessPw,              //-- 3
            "OrderID" => $Order['id'],              //-- 4
            "Method" => "1",                        //-- 5：一括
            "PayTimes" => "1",                      //-- 6：1回
            "CardNo" => "",                         //-- 7
            "Expire" => "",                         //-- 8
            "SecurityCode" => "",                   //-- 9
            "Token" => $token,                      //-- 10
            "HttpAccept" => $httpAccept,            //-- 11
            "HttpUserAgent" => $userAgent,          //-- 12
            "DeviceCategory" => $deviceCategory,    //-- 13
            "ClientField1" => "",                   //-- 14
            "ClientField2" => "",                   //-- 15
            "ClientField3" => "",                   //-- 16
            "ClientFieldFlag" => "0"                //-- 17
        ));
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway のAPIへPOST
    //--------------------------------------------------------------------------------
    private function _postGMOPG($url, $params)
    {   //dump($params);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway エラーが返ってきているか？
    //-- 【030_プロトコルタイプ（カード決済_インターフェイス仕様）_1.20.pdf】
    //-- エラー発生時、ErrCode、ErrInfoが返ってくる
    //--------------------------------------------------------------------------------
    private function _isErrorGMOPG($response)
    {
        foreach($response as $key => $value){
            if($key == 'ErrCode'){
                return true;
            }
        }
        return false;
    }

    //################################################################################
    //-- GMO PaymentGateway エラーコードに対応したエラーメッセージを返す
    //-- 120_エラーコード表_1.74.pdf
    //################################################################################
    public function GMOPG_GetErrorMessage($app, $errorInfos)
    {
        $errors = explode("|", $errorInfos);
        $errorMessages = array();
        foreach($errors as $error){
            $errorMessages[] = self::_GMOPG_GetErrorMessage($app['config']['user_data_realdir'], $error);
        }
        //dump($errors);
        //dump($errorInfos);die();
        return $errorMessages;
    }

    //--------------------------------------------------------------------------------
    //-- GMO PaymentGateway エラーメッセージを返す
    //-- 【120_エラーコード表_1.74.pdf】
    //-- Exx番台：E_error.csv：本サービス決済センター・フロント側アプリケーションにて検知されたエラー、カード決済時発生エラー、決済手段共通エラー（入力パラメータエラー等）
    //-- Mxx番台：M_error.csv：本サービス決済センター・フロント側アプリケーションにて検知されたエラー、カード決済以外の決済手段での発生エラー（入力パラメータエラー等）※一部のエラーはカード決済時にも発生します。
    //-- Cxx番台：C_error.csv：CAFISセンター内でのエラー（他社オーソリエラー、通信エラーなど）
    //-- Gxx番台：G_error.csv：各カード会社内でのエラー（限度額オーバー、有効期限エラーなど）
    //--------------------------------------------------------------------------------
    private function _GMOPG_GetErrorMessage($userDataDir, $error)
    {
        try{
            //-- エラーコード先頭の英字を prefix とする
            $errorCodePrefix = $error[0];   //dump($errorCodePrefix);

            //-- prefix にマッチしたエラーコード表CSVを開く
            $filename = $userDataDir . '/gmopg/' . $errorCodePrefix . '_errors.csv';    //dump($filename); exit();
            $file = new \SplFileObject($filename);    
            $file->setFlags(
                \SplFileObject::READ_CSV | 
                \SplFileObject::READ_AHEAD | 
                \SplFileObject::SKIP_EMPTY | 
                \SplFileObject::DROP_NEW_LINE
            );

            //-- エラーコード表を読み込む
            $errorList = array();
            if (empty($file) == FALSE){
                foreach ($file as $line) {  //dump($line);
                    $errorList[] = $line;
                }
            }   //dump($errorList);

            //-- エラーコードに対応したエラーメッセージを返す
            $count = count($errorList);
            if($count > 0){
                for($i=0; $i<$count; $i++){
                    if($error == $errorList[$i][2]){
                        return $errorList[$i][6];
                    }
                }
            }

            //-- エラーコード表に見つからない場合はエラーコードをそのまま返す
            return $error;
        }
        catch(RuntimeException $e){
            //-- 以下、未対応
            //-- Bxx番台：楽天Edyセンターでのエラー（通信エラーなど）
            //-- Sxx番台：Suicaセンターでのエラー（通信エラーなど）
            //-- Wxx番台：Pay-easy、コンビニ（セブンイレブン以外）センターでのエラー（通信エラーなど）
            //-- Dxx番台：コンビニ（セブンイレブン）センターでのエラー（通信エラーなど）
            //-- Fxx番台：不正住所検知サービスでのエラー
            //-- Pxx番台：PayPalセンターでのエラー
            //-- Nxx番台：DoCoMo認証支援センターでのエラー
            //-- WMx番台：WebMoneyセンターでのエラー
            //-- Aux番台：auセンターでのエラー
            //-- AMP：auセンターでのエラー
            //-- DCx番台：ドコモセンターでのエラー
            //-- SBx番台：ソフトバンクセンターでのエラー
            //-- Jxx番台：じぶん銀行センターでのエラー
            //-- NCxx番台：NET CASHセンターでのエラー
            //-- RIxx番台：楽天ペイでのエラー
            //-- LPxx番台：LINE Pay決済でのエラー
            //-- UPxx番台：ネット銀聯決済でのエラー
            //-- SCxx番台：ソフトバンクまとめて支払い（B）継続課金決済でのエラー
            //-- SExx番台：コンビニ決済（セブンイレブン）でのエラー
            //-- RC1,RC2番台：リクルートかんたん支払い決済でのエラー
            //-- RC3,RC4番台：リクルートかんたん支払い継続課金決済でのエラー
            //-- LWx番台：コンビニ決済（ローソン・ミニストップ）でのエラー
            //-- FMx番台：コンビニ決済（ファミリーマート）でのエラー
            //-- ACx番台：口座振替、口座振替（セレクト）でのエラー
            //throw $e;
            return $e->GetMessage();
        }

    }
}
