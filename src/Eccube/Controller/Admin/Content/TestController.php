<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller\Admin\Content;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Master\CsvType;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//use Eccube\Util\FormUtil;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class TestController extends AbstractController
{
    public function index(Application $app, Request $request)
    {
        $NoticeList = $app['eccube.repository.notice']->findBy(array(), array('rank' => 'DESC'));
        
        $builder = $app->form();
        
        $event = new EventArgs(
            array(
                'builder' => $builder,
                'NoticeList' => $NoticeList,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_CONTENT_NOTICE_INDEX_INITIALIZE, $event);
        
        $form = $builder->getForm();
        
        return $app->render('Content/notice.twig', array(
            'form' => $form->createView(),
            'NoticeList' => $NoticeList,
        ));
    }

    public function edit(Application $app, Request $request, $id = null)
    {
        if($id){
            $Notice = $app['eccube.repository.notice']->find($id);
            if(!$Notice){
                throw new NotFoundHttpException();
            }
            $Notice->setLinkMethod((bool) $Notice->getLinkMethod());
        }
        else{
            $Notice = new \Eccube\Entity\Notice();
        }

        $builder = $app['form.factory']
            ->createBuilder('admin_notice', $Notice);

        $form = $builder->getForm();
            
        // ファイルの登録
        $images = array();

        $NoticeImages = $Notice->getNoticeImage();
        foreach ($NoticeImages as $NoticeImage) {
            $images[] = $NoticeImage->getFileName();
        }

        $form['images']->setData($images);

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Notice' => $Notice,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_CONTENT_NOTICE_EDIT_INITIALIZE, $event);

        if('POST' === $request->getMethod()){
            $form->handleRequest($request);
            if($form->isValid()){
                $data = $form->getData();
                if(empty($data['url'])){
                    $Notice->setLinkMethod(Constant::DISABLED);
                }

                $status = $app['eccube.repository.notice']->save($Notice);

                // 画像の登録
                $add_images = $form->get('add_images')->getData();
                foreach ($add_images as $add_image) {
                    $NoticeImage = new \Eccube\Entity\NoticeImage();
                    $NoticeImage
                        ->setFileName($add_image)
                        ->setNotice($Notice)
                        ->setRank(1);
                    $Notice->addNoticeImage($NoticeImage);
                    $app['orm.em']->persist($NoticeImage);

                    // 移動
                    $file = new File($app['config']['image_temp_realdir'].'/'.$add_image);
                    $file->move($app['config']['image_save_realdir']);
                }

                // 画像の削除
                $delete_images = $form->get('delete_images')->getData();
                foreach ($delete_images as $delete_image) {
                    $NoticeImage = $app['eccube.repository.notice_image']
                        ->findOneBy(array('file_name' => $delete_image));

                    // 追加してすぐに削除した画像は、Entityに追加されない
                    if ($NoticeImage instanceof \Eccube\Entity\NoticeImage) {
                        $Notice->removeNoticeImage($NoticeImage);
                        $app['orm.em']->remove($NoticeImage);
                    }
                    $app['orm.em']->persist($Notice);

                    // 削除
                    if (!empty($delete_image)) {
                        $fs = new Filesystem();
                        $fs->remove($app['config']['image_save_realdir'].'/'.$delete_image);
                    }
                }
                $app['orm.em']->persist($Notice);
                $app['orm.em']->flush();


                $ranks = $request->get('rank_images');
                if ($ranks) {
                    foreach ($ranks as $rank) {
                        list($filename, $rank_val) = explode('//', $rank);
                        $NoticeImage = $app['eccube.repository.notice_image']
                            ->findOneBy(array(
                                'file_name' => $filename,
                                'Notice' => $Notice,
                            ));
                        $NoticeImage->setRank($rank_val);
                        $app['orm.em']->persist($NoticeImage);
                    }
                }
                $app['orm.em']->flush();

                if($status){
                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'Notice' => $Notice,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_CONTENT_NOTICE_EDIT_COMPLETE, $event);

                    $app->addSuccess('admin.notice.save.complete', 'admin');

                    return $app->redirect($app->url('admin_content_notice'));
                }
                $app->addError('admin.notice.save.error', 'admin');
            }
        }

        return $app->render('Content/notice_edit.twig', array(
            'form' => $form->createView(),
            'Notice' => $Notice,
        ));
    }

    public function up(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        $TargetNotice = $app['eccube.repository.notice']->find($id);
        if(!$TargetNotice){
            throw new NotFoundHttpException();
        }

        $status = $app['eccube.repository.notice']->up($TargetNotice);

        if($status){
            $app->addSuccess('admin.notice.up.complete', 'admin');
        }
        else{
            $app->addError('admin.notice.up.error', 'admin');
        }

        return $app->redirect($app->url('admin_content_notice'));
    }

    public function down(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        $TargetNotice = $app['eccube.repository.notice']->find($id);
        if(!$TargetNotice){
            throw new NotFoundHttpException();
        }

        $status = $app['eccube.repository.notice']->down($TargetNotice);

        if($status){
            $app->addSuccess('admin.notice.down.complete', 'admin');
        }
        else{
            $app->addError('admin.notice.down.error', 'admin');
        }

        return $app->redirect($app->url('admin_content_notice'));
    }

    public function delete(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        $TargetNotice = $app['eccube.repository.notice']->find($id);
        if(!$TargetNotice){
            throw new NotFoundHttpException();
        }

        $status = $app['eccube.repository.notice']->delete($TargetNotice);

        $event = new EventArgs(
            array(
                'TargetNotice' => $TargetNotice,
                'status' => $status,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_CONTENT_NOTICE_DELETE_COMPLETE, $event);
        $status = $event->getArgument('status');

        if($status){
            $app->addSuccess('admin.notice.delete.complete', 'admin');
        }
        else{
            $app->addSuccess('admin.notice.delete.error', 'admin');
        }

        return $app->redirect($app->url('admin_content_notice'));
    }







    public function addImage(Application $app, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('リクエストが不正です');
        }

        $images = $request->files->get('admin_notice');

        $files = array();
        if (count($images) > 0) {
            foreach ($images as $img) {
                foreach ($img as $image) {
                    //ファイルフォーマット検証
                    $mimeType = $image->getMimeType();
                    if (0 !== strpos($mimeType, 'image')) {
                        throw new UnsupportedMediaTypeHttpException('ファイル形式が不正です');
                    }

                    $extension = $image->getClientOriginalExtension();
                    $filename = date('mdHis').uniqid('_').'.'.$extension;
                    $image->move($app['config']['image_temp_realdir'], $filename);
                    $files[] = $filename;
                }
            }
        }

        $event = new EventArgs(
            array(
                'images' => $images,
                'files' => $files,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_CONTENT_NOTICE_ADD_IMAGE_COMPLETE, $event);
        $files = $event->getArgument('files');

        return $app->json(array('files' => $files), 200);
    }
}
