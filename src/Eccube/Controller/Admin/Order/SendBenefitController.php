<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Controller\Admin\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Master\DeviceType;
use Eccube\Entity\OrderDetail;
use Eccube\Entity\ShipmentItem;
use Eccube\Entity\Shipping;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Util\BenefitUtil;
use Eccube\Entity\MailHistory;

class SendBenefitController extends AbstractController
{
/*****
    private function getQuantity($quantities, $id)
    {
        foreach($quantities as $key => $value){
            if($key == $id){
                return $value;
            }
        }
        return 0;
    }
*****/

    public function index(Application $app, Request $request, $id = null)
    {
        $TargetOrder = null;

        if (is_null($id)) {
            // 空のエンティティを作成.
            $TargetOrder = $this->newOrder($app);
        } else {
            $TargetOrder = $app['eccube.repository.order']->find($id);
            if (is_null($TargetOrder)) {
                throw new NotFoundHttpException();
            }
        }

        // Save previous value before calculate
        $arrOldOrder = array();
        $quantities = array();

        /** @var $OrderDetail OrderDetail*/
        foreach ($TargetOrder->getOrderDetails() as $OrderDetail) {
            $arrOldOrder['OrderDetails'][$OrderDetail->getId()]['quantity'] = $OrderDetail->getQuantity();
            $quantities[$OrderDetail->getId()] = $OrderDetail->getQuantity();
        }
//dump($quantities);

        $builder = $app['form.factory']->createBuilder('order', $TargetOrder);
        $form = $builder->getForm();





        //-- 入金済みを再確認
        if($TargetOrder['OrderStatus']['id'] == 6){
/*****
            $benefits = BenefitUtil::GetBenefitForUpdate(
                $id,
                $this->getQuantity($quantities, $id)
            );
            if(($benefits != null) || (count($benefits) > 0)){
                BenefitUtil::DeliveredBenefit($id);

                //-- 優待番号メール送信
                $MailHistory = BenefitUtil::SendMail($app, $benefits, $TargetOrder);
            }
*****/

            foreach ($TargetOrder->getOrderDetails() as $OrderDetail) {
//dump($OrderDetail->getProductCode());
//dump($OrderDetail->getQuantity());
                $benefits[$OrderDetail->getProductCode()] = BenefitUtil::GetBenefitForUpdateNew(
                    $id,
                    $OrderDetail->getProductCode(),
                    $OrderDetail->getQuantity()
                );
            }   //dump($benefitsNew);

            if($this->isValidSendBenefits($benefits)){
                BenefitUtil::DeliveredBenefit($id);

                //-- 優待番号メール送信
                $MailHistory = BenefitUtil::SendMailNew($app, $benefits, $TargetOrder);
            }
            else{
                BenefitUtil::RevertBenefit($id);

                //-- order_detail のどれかが優待番号を取得出来ているかもしれないので、優待番号情報を null にする
                //-- order の、全ての優待番号を取得出来なかった場合、無効とする仕様の為
                foreach($benefits as $benefit){
                    $benefit = null;
                }

                //-- 優待番号空きなしメール送信（Adminでは管理者へメールを送信しない）
                //$MailHistory = BenefitUtil::SendEmptyMail($app, $TargetOrder);
            }
        }





        return $app->render('Order/benefit.twig', array(
            'form' => $form->createView(),
            'Order' => $TargetOrder,
            'id' => $id,
            'arrOldOrder' => $arrOldOrder,
            'Benefits' => $benefits,
        ));
    }

    //--------------------------------------------------------------------------------
    //-- メール送信する優待番号は取得出来ているか
    //--------------------------------------------------------------------------------
    private function isValidSendBenefits($benefits)
    {
        foreach($benefits as $benefit){
            if($benefit == null){
                return false;   //-- 複数の商品コードのうち、一つでも取得出来なかった場合は false を返す
            }
        }
        return true;
    }
}
