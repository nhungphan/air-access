<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Controller\Admin\Benefit;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Entity\MailHistory;

class BenefitEditController extends AbstractController
{
    public function index(Application $app, Request $request, $id = null)
    {
        $app['orm.em']->getFilters()->enable('incomplete_order_status_hidden');
        // 編集
        if ($id) {
            $Benefit = $app['orm.em']
                ->getRepository('Eccube\Entity\Benefit')
                ->find($id);

            if (is_null($Benefit)) {
                throw new NotFoundHttpException();
            }
        // 新規登録
        } else {
            $Benefit = $app['eccube.repository.benefit']->newBenefit();
        }

//dump($Benefit);
        //-- order_id が入っている場合、order_id から 受注情報を取得する
        $orderId = $Benefit['order_id'];
        if(!is_null($orderId)){
//dump($orderId);
            $Order = $app['orm.em']
                ->getRepository('Eccube\Entity\Order')
                ->find($orderId);
//dump($Order);
        }

        // 優待番号登録フォーム
        $builder = $app['form.factory']
            ->createBuilder('admin_benefit', $Benefit);

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Benefit' => $Benefit,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_BENEFIT_EDIT_INDEX_INITIALIZE, $event);

        $form = $builder->getForm();

//dump($Benefit);
//dump($request->get('delete_flag'));
//dump($request->get('mail_delivered'));
        if($this->isDelete($request)){
            //-- 削除する
            $Benefit->setDelFlg(Constant::ENABLED);
            $app['orm.em']->persist($Benefit);
            $app['orm.em']->flush();

            $app->addSuccess('admin.benefit.delete.complete', 'admin');

            $session = $request->getSession();
            $page_no = intval($session->get('eccube.admin.benefit.search.page_no'));
            $page_no = $page_no ? $page_no : Constant::ENABLED;

            return $app->redirect($app->url('admin_benefit_page', array('page_no' => $page_no)).'?resume='.Constant::ENABLED);
        }
        else{
            if ('POST' === $request->getMethod()) {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    log_info('優待番号登録開始', array($Benefit->getId()));

                    if($this->isDelivered($request)){
                        $Benefit->setDoneDate(new \DateTime());

                        // メール送信
                        $MailHistory = $this->sendMail($app, $Benefit, $Order);
                    }

                    $app['orm.em']->persist($Benefit);
                    $app['orm.em']->flush();

                    log_info('優待番号登録完了', array($Benefit->getId()));

                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'Benefit' => $Benefit,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_BENEFIT_EDIT_INDEX_COMPLETE, $event);

                    $app->addSuccess('admin.benefit.save.complete', 'admin');

                    return $app->redirect($app->url('admin_benefit_edit', array(
                        'id' => $Benefit->getId(),
                    )));
                } else {
                    $app->addError('admin.benefit.save.failed', 'admin');
                }
            }

            return $app->render('Benefit/edit.twig', array(
                'form' => $form->createView(),
                'Benefit' => $Benefit,
            ));
        }
    }

    //-- 削除
    private function isDelete($request)
    {
        return ($request->get('delete_flag') == '1');
    }

    //-- メール送信完了
    private function isDelivered($request)
    {
        return ($request->get('mail_delivered') == '1');
    }

    //-- メール送信
    public function sendMail($app, $Benefit, $Order)
    {
        // メール送信
        $message = $app['eccube.service.mail']->sendBenefitMailAirAccess($Order, array($Benefit->getBenefitCode()));

        // 送信履歴を保存.
        $MailTemplate = $app['eccube.repository.mail_template']->find(7);   //-- 優待番号送信メールテンプレートは '7'

        $MailHistory = new MailHistory();
        $MailHistory
            ->setSubject($message->getSubject())
            ->setMailBody($message->getBody())
            ->setMailTemplate($MailTemplate)
            ->setSendDate(new \DateTime())
            ->setOrder($Order);

        $app['orm.em']->persist($MailHistory);
        $app['orm.em']->flush($MailHistory);

        return $MailHistory;
    }
}
