<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller\Admin\Benefit;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Master\CsvType;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Entity\MailHistory;

class BenefitController extends AbstractController
{
    public function index(Application $app, Request $request, $page_no = null)
    {
        $session = $request->getSession();
        $pagination = array();
        $builder = $app['form.factory']
            ->createBuilder('admin_search_benefit');

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_BENEFIT_INDEX_INITIALIZE, $event);

        $searchForm = $builder->getForm();

        //アコーディオンの制御初期化( デフォルトでは閉じる )
        $active = false;

        $pageMaxis = $app['eccube.repository.master.page_max']->findAll();

        // 表示件数は順番で取得する、1.SESSION 2.設定ファイル
        $page_count = $session->get('eccube.admin.benefit.search.page_count', $app['config']['default_page_count']);

        $page_count_param = $request->get('page_count');
        // 表示件数はURLパラメターから取得する
        if($page_count_param && is_numeric($page_count_param)){
            foreach($pageMaxis as $pageMax){
                if($page_count_param == $pageMax->getName()){
                    $page_count = $pageMax->getName();
                    // 表示件数入力値正し場合はSESSIONに保存する
                    $session->set('eccube.admin.benefit.search.page_count', $page_count);
                    break;
                }
            }
        }

        $undone = 0;
        $done = 0;

        if ('POST' === $request->getMethod()) {

            $searchForm->handleRequest($request);

            if ($searchForm->isValid()) {
                $searchData = $searchForm->getData();

//dump($searchData);
                $undone = is_null($request->get('undone')) ? 0 : 1;
                $done = is_null($request->get('done')) ? 0 : 1;
                $searchData = array_merge($searchData, array('undone' => $undone, 'done' => $done));
//dump($searchData);

                // paginator
                $qb = $app['eccube.repository.benefit']->getQueryBuilderBySearchDataForAdmin($searchData);
                $page_no = 1;

                $event = new EventArgs(
                    array(
                        'form' => $searchForm,
                        'qb' => $qb,
                    ),
                    $request
                );
                $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_BENEFIT_INDEX_SEARCH, $event);

                $pagination = $app['paginator']()->paginate(
                    $qb,
                    $page_no,
                    $page_count
                );

                // sessionに検索条件を保持.
                $viewData = \Eccube\Util\FormUtil::getViewData($searchForm);
                $viewData = array_merge($viewData, array('undone' => $undone, 'done' => $done));
//dump($viewData);
                $session->set('eccube.admin.benefit.search', $viewData);
                $session->set('eccube.admin.benefit.search.page_no', $page_no);
            }
        } else {
            if (is_null($page_no) && $request->get('resume') != Constant::ENABLED) {
                // sessionを削除
                $session->remove('eccube.admin.benefit.search');
                $session->remove('eccube.admin.benefit.search.page_no');
                $session->remove('eccube.admin.benefit.search.page_count');
            } else {
                // pagingなどの処理
                if (is_null($page_no)) {
                    $page_no = intval($session->get('eccube.admin.benefit.search.page_no'));
                } else {
                    $session->set('eccube.admin.benefit.search.page_no', $page_no);
                }
                $viewData = $session->get('eccube.admin.benefit.search');
                if (!is_null($viewData)) {
                    // sessionに保持されている検索条件を復元.
                    $searchData = \Eccube\Util\FormUtil::submitAndGetData($searchForm, $viewData);

//dump($viewData);
                    //-- 'undone' と 'done' を searchData に突っ込む
                    $undone = $viewData['undone'];
                    $done = $viewData['done'];
                    $searchData['undone'] = $undone;
                    $searchData['done'] = $done;
//dump($searchData);

                    // 表示件数
                    $page_count = $request->get('page_count', $page_count);

                    $qb = $app['eccube.repository.benefit']->getQueryBuilderBySearchDataForAdmin($searchData);

                    $event = new EventArgs(
                        array(
                            'form' => $searchForm,
                            'qb' => $qb,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::ADMIN_BENEFIT_INDEX_SEARCH, $event);

                    $pagination = $app['paginator']()->paginate(
                        $qb,
                        $page_no,
                        $page_count
                    );
                }
            }
        }

        return $app->render('Benefit/index.twig', array(
            'searchForm' => $searchForm->createView(),
            'pagination' => $pagination,
            'pageMaxis' => $pageMaxis,
            'page_no' => $page_no,
            'page_count' => $page_count,
            'active' => $active,
            'undone' => $undone,
            'done' => $done,
        ));
    }

    //-- 削除
    public function delete(Application $app, Request $request)
    {
        log_info('優待番号一括削除開始');

        $session = $request->getSession();
        $page_no = intval($session->get('eccube.admin.benefit.search.page_no'));
        $page_no = $page_no ? $page_no : Constant::ENABLED;

//dump($request);
        $ids = explode(',', $request->get('deleteIds'));
        foreach($ids as $id){
            log_info('優待番号削除', array($id));
//dump($id);
            $Benefit = $app['orm.em']
                ->getRepository('Eccube\Entity\Benefit')
                ->find($id);

            if (!$Benefit) {
                log_info('優待番号が見つからない', array($id));
            }
            else{
                $Benefit->setDelFlg(Constant::ENABLED);
                $app['orm.em']->persist($Benefit);
                $app['orm.em']->flush();
            }
        }

        log_info('優待番号一括削除完了');

        $app->addSuccess('admin.benefit.delete.complete', 'admin');

        return $app->redirect($app->url('admin_benefit_page', array('page_no' => $page_no)).'?resume='.Constant::ENABLED);
    }

    //-- メール送信完了
    public function delivered(Application $app, Request $request)
    {
        log_info('優待番号一括メール送信完了処理開始');

        $session = $request->getSession();
        $page_no = intval($session->get('eccube.admin.benefit.search.page_no'));
        $page_no = $page_no ? $page_no : Constant::ENABLED;

        $now = new \DateTime();  //date('Y-m-d H:i:s');

//dump($request);
        $ids = explode(',', $request->get('deliveredIds'));

        log_info('優待番号メール送信開始');
        $list = $this->orderBasedAggregation($app, $ids);   //-- order_id に紐づく email で寄せたリストを作成する
        //-- $list --- [0] --- orderId
        //--        |       +- email
        //--        |       +- benefits --- benefitCode[0]
        //--        |                    +- benefitCode[1]
        //--        +- [1] --- orderId
        //--                +- email
        //--                +- benefits --- benefitCode[0]
//dump($list);
        foreach($list as $item){
            $orderId = $item['orderId'];

            $Order = $app['orm.em']
                ->getRepository('Eccube\Entity\Order')
                ->find($orderId);

            if (!$Order) {
                log_info('受注番号が見つからない', array($orderId));
            }
            else{
                $this->sendMail($app, $Order, $item['benefits']);
            }
        }
        log_info('優待番号メール送信完了');

        log_info('優待番号メール送信完了日更新開始');
        foreach($ids as $id){
//dump($id);
            $Benefit = $app['orm.em']
                ->getRepository('Eccube\Entity\Benefit')
                ->find($id);

            if (!$Benefit) {
                log_info('優待番号が見つからない', array($id));
            }
            else{
                $orderId = $Benefit['order_id'];

                if(!is_null($orderId)){
                    log_info('優待番号メール送信完了日更新', array($id));
                    $Benefit->setDoneDate($now);
//dump($Benefit);
                    $app['orm.em']->persist($Benefit);
                    $app['orm.em']->flush();
//dump($Benefit);
                }
                else{
                    //-- order_id を持たない場合は対象から外す
                }
            }
        }
        log_info('優待番号メール送信完了日更新完了');

        log_info('優待番号一括メール送信完了処理完了');

        $app->addSuccess('admin.benefit.delivered.complete', 'admin');

        return $app->redirect($app->url('admin_benefit_page', array('page_no' => $page_no)).'?resume='.Constant::ENABLED);
    }

    //-- 寄せ
    private function isExistNonmember(&$nonmembers, $orderId, $email, $benefitCode)
    {
        $count = count($nonmembers);
        for($i=0; $i<$count; $i++){
            //-- order_id と email が既に存在していた場合、benefit_code を追加して true を返す
            if(($nonmembers[$i]['orderId'] == $orderId) && ($nonmembers[$i]['email'] == $email)){
                $nonmembers[$i]['benefits'][] = $benefitCode;
                return true;
            }
        }
        return false;
    }

    //-- benefitをorder_idで寄せる
    private function orderBasedAggregation($app, $benefitIds)
    {
        $nonmembers = array();

        foreach($benefitIds as $benefitId){
            $Benefit = $app['orm.em']
                ->getRepository('Eccube\Entity\Benefit')
                ->find($benefitId);

            if (!$Benefit) {
                log_info('優待番号が見つからない', array($benefitId));
            }
            else{
                $benefitCode = $Benefit['benefit_code'];
                $orderId = $Benefit['order_id'];

                if(!is_null($orderId)){
                    $Order = $app['orm.em']
                        ->getRepository('Eccube\Entity\Order')
                        ->find($orderId);

                    if (!$Order) {
                        log_info('受注番号が見つからない', array($orderId));
                    }
                    else{
                        $email = $Order['email'];

                        //-- order_id と email が未だ存在していない場合、order_id, email, benefit_code を追加する
                        if(!$this->isExistNonmember($nonmembers, $orderId, $email, $benefitCode)){
                            $nonmembers[] = array('orderId' => $orderId, 'email' => $email, 'benefits' => array($benefitCode));
                        }
                    }
                }
                else{
                    //-- order_id を持たない場合は対象から外す
                }
            }
        }

        return $nonmembers;
    }

    //-- メール送信
    public function sendMail($app, $Order, $benefits)
    {
        // メール送信
        $message = $app['eccube.service.mail']->sendBenefitMailAirAccess($Order, $benefits);

        // 送信履歴を保存.
        $MailTemplate = $app['eccube.repository.mail_template']->find(7);   //-- 優待番号送信メールテンプレートは '7'

        $MailHistory = new MailHistory();
        $MailHistory
            ->setSubject($message->getSubject())
            ->setMailBody($message->getBody())
            ->setMailTemplate($MailTemplate)
            ->setSendDate(new \DateTime())
            ->setOrder($Order);

        $app['orm.em']->persist($MailHistory);
        $app['orm.em']->flush($MailHistory);

        return $MailHistory;
    }
}
