<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AirAccessInputConfirmController extends AbstractController
{

    /**
     * 入力確認画面へ既存会員の情報を表示
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {
      $builder = $app['form.factory']->createBuilder('air_access_member');

      $paths = array($app['config']['user_data_realdir']);
      $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

      $form = $builder->getForm();
      $form->handleRequest($request);

      $data = $form->getData();

      $session = $app['session'];
      $customer = $session->get('customer');

      if(empty($customer['kana01'])){
        $err_flg = 1;
        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_login_form.twig' : 'air_access_sp_login_form.twig';
        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));
        return $app->render($template, array('err_flg' => $err_flg));
      }

      $data = array();
      $data['name01'] = $customer['name01'];
      $data['name02'] = $customer['name02'];
      $data['kana01'] = $customer['kana01'];
      $data['kana02'] = $customer['kana02'];
      $data['email'] = $customer['email'];
      $data['repeated_email'] = "";
      $data['tel01'] = $customer['tel01'];
      $data['tel02'] = $customer['tel02'];
      $data['tel03'] = $customer['tel03'];
      $data['receipt'] = "";
      $data['note'] = "";
      $data['receipt'] = "";
      $data['message'] = "";

      $form->setData($data);

      //-- 支払い方法一覧を取得
      $payments = $app['eccube.repository.payment']
          ->findBy(
              array('del_flg' => 0),
              array('rank' => 'DESC')
      );  //dump($payments);


      $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input.twig' : 'air_access_sp_input.twig';
      return $app->render(
        $template,
          array(
            'form' => $form->createView(),
            'nonmember' => '1',
            'payments' => $payments,
            'paymentSelected' => 0,
            'token' => '',
            'maskedCardNumber' => '',
          )
      );

    }



}
