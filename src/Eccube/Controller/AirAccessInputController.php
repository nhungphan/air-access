<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Entity\Customer;
use Eccube\Entity\CustomerAddress;
use Eccube\Entity\ShipmentItem;
use Eccube\Entity\Shipping;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;
use Eccube\Exception\ShoppingException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

use Eccube\Util\GmoPg;
use Eccube\Util\BenefitUtil;
use Eccube\Entity\MailHistory;

class AirAccessInputController extends AbstractController
{

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionKey = 'eccube.front.shopping.nonmember';

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionCustomerAddressKey = 'eccube.front.shopping.nonmember.customeraddress';

    /**
     * @var string 複数配送警告メッセージ
     */
    private $sessionMultipleKey = 'eccube.front.shopping.multiple';

    /**
     * @var string 受注IDキー
     */
    private $sessionOrderKey = 'eccube.front.shopping.order.id';

    //-- @@@ ▼ air-access
    /**
     * @var string GMO PaymentGateway エラーコード
     */
    private $sessionGMOPG_ErrorsKey = 'eccube.front.gmopg.errors';

    /**
     * @var string GMO PaymentGateway エラーメッセージ
     */
    private $sessionGMOPG_ErrorMessagesKey = 'eccube.front.gmopg.error.messages';
     
    /**
     * @var string 3D Auth スルーオブジェクト
     */
    private $session3dAuth_ThroughObjKey = 'eccube.front.3dauth.through';
    //-- @@@ ▲ air-access

    //--------------------------------------------------------------------------------
    //-- 支払い方法一覧を返す
    //--------------------------------------------------------------------------------
    private function _getPayments($app)
    {
        return $app['eccube.repository.payment']->findBy(
            array('del_flg' => 0),
            array('rank' => 'DESC')
        );
    }

    //--------------------------------------------------------------------------------
    //-- 選択された payment_id のオブジェクトを返す
    //--------------------------------------------------------------------------------
    private function _getSelectedPayment($Payments, $id)
    {
        foreach($Payments as $value){
            if($value['id'] == $id){
                return $value;
            }
        }
        return null;
    }

    //--------------------------------------------------------------------------------
    //-- 購入処理
    //--------------------------------------------------------------------------------
    private function _execPurchase($app, $Order)
    {
        //-- トランザクション制御
        $em = $app['orm.em'];
        $em->getConnection()->beginTransaction();

        try{
            //-- お問い合わせ、配送時間などのフォーム項目をセット
            //$app['eccube.service.shopping']->setFormData($Order, $data);

            //-- 購入処理
            $app['eccube.service.shopping']->processPurchase($Order);

            $em->flush();
            $em->getConnection()->commit();
            
            log_info('購入処理完了', array($Order->getId()));
        }
        catch(ShoppingException $e){
            log_error('購入エラー', array($e->getMessage()));

            $em->getConnection()->rollback();

            $app->log($e);
            $app->addError($e->getMessage());

            return $app->redirect($app->url('shopping_error'));
        }
        catch(\Exception $e){
            log_error('予期しないエラー', array($e->getMessage()));

            $em->getConnection()->rollback();

            $app->log($e);
            $app->addError('front.shopping.system.error');

            return $app->redirect($app->url('shopping_error'));
        }

        //-- カート削除
        $app['eccube.service.cart']->clear()->save();

        //-- 受注IDをセッションにセット
        $app['session']->set($this->sessionOrderKey, $Order->getId());
    }

    //--------------------------------------------------------------------------------
    //-- クレジット決済例外処理
    //--------------------------------------------------------------------------------
    private function _creditCardPaymentException($app, $Order, $errors, $errorMessages)
    {
        $Order->setGMO_Errors($errors);

        $Order->setOrderDate(new \DateTime()); //-- @@@ 受注日時を設定（正確には受注ではないか？）
        $Order->setOrderStatus($app['eccube.repository.master.order_status']->find(1)); //-- @@@ 決済処理中にしておく

        log_error('クレジット決済エラー', array($errors));

        //-- トランザクション制御
        $em = $app['orm.em'];
        $em->getConnection()->beginTransaction();

        $em->flush();
        $em->getConnection()->commit();

        $app->log($errors);
        $app->addError($errors);

        //-- カート削除
        $app['eccube.service.cart']->clear()->save();

        //-- 受注IDをセッションにセット
        $app['session']->set($this->sessionOrderKey, $Order->getId());
        //-- エラーコードをセッションにセット
        $app['session']->set($this->sessionGMOPG_ErrorsKey, $errors);
        //-- エラーメッセージをセッションにセット
        $app['session']->set($this->sessionGMOPG_ErrorMessagesKey, $errorMessages);
        
        return $app->redirect($app->url('input_error'));
    }

    //--------------------------------------------------------------------------------
    //-- メイン制御
    //--------------------------------------------------------------------------------
    public function index(Application $app, Request $request)
    {
//dump($request);
        $cartService = $app['eccube.service.cart'];

        //-- カートチェック：カートがロックされていない時はエラー
        if (!$cartService->isLocked()) {
            log_info('カートが存在しません'); //dump('カートが存在しない、カートがロックされていない'); exit;
            return $app->redirect($app->url('cart'));
        }

        //-- カートチェック：カートが存在しない時はエラー
        if (count($cartService->getCart()->getCartItems()) <= 0) {
            log_info('カートに商品が入っていないためショッピングカート画面にリダイレクト');  //dump('カートに商品が入っていない'); exit;
            return $app->redirect($app->url('cart'));
        }

        $builder = $app['form.factory']->createBuilder('air_access_member');
        $form = $builder->getForm();
        $form->handleRequest($request);

        //-- 非会員フラグをセット
        $nonmember = $request->get('nonmember');    //dump($nonmember);

        //-- 選択された支払い方法の id をセット
        $paymentId = (!is_null($request->get('paymentSelector'))) ? $request->get('paymentSelector') : $request->get('paymentSelected');    //dump($paymentId);

        //-- カード情報のトークン、マスクされたカード番号をセット
        $token = $request->get('token');
        $maskedCardNumber = $request->get('maskedCardNumber');

        //-- 支払い方法をセット
        $payments = $this->_getPayments($app);
        $selectedPayment = $this->_getSelectedPayment($payments, $paymentId); //dump($selectedPayment);

        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();   //dump($data);

            switch ($request->get('mode')) {
                case 'confirm':
                {
                    $builder->setAttribute('freeze', true);
                    $form = $builder->getForm();
                    $form->handleRequest($request);

                    $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input_confirm.twig' : 'air_access_sp_input_confirm.twig';
                    return $app->render($template, array(
                        'form' => $form->createView(),
                        'nonmember' => $nonmember,
                        //'payments' => $payments,  //-- @@@ 確認画面で支払い方法を選択させる場合は有効化する
                        'paymentMethod' => $selectedPayment['Method'],
                        'paymentSelected' => $paymentId,
                        'token' => $token,
                        'maskedCardNumber' => $maskedCardNumber,
                    ));
                }

                case 'complete':
                {
                    if($nonmember == '1'){
                        //-- @@@ ▼ 以下、EC-CUBE定型
                        {
                            log_info('非会員お客様情報登録開始');

                            $customerId = 0;
                            $session = $app['session'];
                            if($session != null){
                                $customer = $session->get('customer');
                                if($customer != null){
                                    //dump($customer);
                                    //dump($customer['id']);exit;
                                    $customerId = $customer['id'];
                                }
                            }

                            $Customer = new Customer();
                            $Customer
                                ->setName01($data['name01'])
                                ->setName02($data['name02'])
                                ->setKana01($data['kana01'])
                                ->setKana02($data['kana02'])
                                ->setEmail($data['email'])
                                ->setTel01($data['tel01'])
                                ->setTel02($data['tel02'])
                                ->setTel03($data['tel03']);

                            //-- 非会員複数配送用
                            $CustomerAddress = new CustomerAddress();
                            $CustomerAddress
                                ->setCustomer($Customer)
                                ->setName01($data['name01'])
                                ->setName02($data['name02'])
                                ->setKana01($data['kana01'])
                                ->setKana02($data['kana02'])
                                ->setTel01($data['tel01'])
                                ->setTel02($data['tel02'])
                                ->setTel03($data['tel03'])
                                ->setDelFlg(Constant::DISABLED);

                            $Customer->addCustomerAddress($CustomerAddress);

                            //-- 受注情報を取得
                            $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);

                            //-- 初回アクセス(受注データがない)の場合は, 受注情報を作成
                            if (is_null($Order)) {
                                //-- 受注情報を作成
                                try{
                                    //-- 受注情報を作成
                                    $Order = $app['eccube.service.shopping']->createOrder($Customer);
                                }
                                catch(CartException $e){
                                    $app->addRequestError($e->getMessage());
                                    return $app->redirect($app->url('cart'));
                                }
                            }

                            //-- 非会員用セッションを作成
                            $nonMember = array();
                            $nonMember['customer'] = $Customer;
                            //-- @@@ ▼ air-access では住所情報は扱わない
                            //$nonMember['pref'] = $Customer->getPref()->getId();
                            $app['session']->set($this->sessionKey, $nonMember);

                            log_info('非会員お客様情報登録完了', array($Order->getId()));
                        }
                        //-- @@@ ▲ 以上、EC-CUBE定型

                        //-- アクセスチケット会員IDでログインしていたら、そのIDをセット
                        if($customerId != 0){
                            $Order->setAccessTicketCustomerId($customerId);
                        }

//dump($Order);
                        //-- 領収書と備考をセット
                        $Order->setReceipt($data['receipt']);
                        $Order->setMessage($data['message']);

                        //-- 選択された支払方法をセット
                        $Order->setPayment($selectedPayment);
                        $Order->setPaymentMethod($selectedPayment['Method']);
                        $Order->setCharge($selectedPayment['Charge']);
//dump($Order);

                        $resultGMOPG = null;
                        //-- 選択された支払い方法がクレジットカード（payment_id:5）の場合、GMO PG で決済処理
                        if($selectedPayment['id'] === 5){
//*****
                            try{
                                $resultGMOPG = GmoPg::ExecGMOPG(
                                    $Order,
                                    $request,
                                    $token,
                                    $this->isPC($request->headers->get('User-Agent')) ? 0 : 1
                                ); //dump($resultGMOPG);    exit();
                                
                                //-- 本人認証パスワード入力URLが返ってきた場合は本人認証パスワード入力画面（3D Auth）へのリダイレクト画面を表示しリダイレクトする
                                if(array_key_exists('ACSUrl', $resultGMOPG)) {
                                    //-- 本人認証後に使用する情報をセッションに格納する
                                    $app['session']->set($this->session3dAuth_ThroughObjKey, array(
                                        'receipt' => $data['receipt'],
                                        'message' => $data['message'],
                                        'paymentId' => $paymentId,
                                    ));

                                    $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_3d_auth.twig' : 'air_access_sp_3d_auth.twig';
                                    return $app->render($template, array(
                                        'ACSUrl' => $resultGMOPG['ACSUrl'],
                                        'PaReq' => $resultGMOPG['PaReq'],
                                        'TermUrl' => 'auth_done',           //-- 認証後URL
                                        'MD' => $resultGMOPG['MD'],
                                    ));
                                }

                                $Order->setGMO_Approve($resultGMOPG['Approve']);
                                $Order->setGMO_TranID($resultGMOPG['TranID']);
                                $Order->setGMO_TranDate($resultGMOPG['TranDate']);
                                $Order->setGMO_CheckSum($resultGMOPG['CheckString']);

                                //-- クレジット決済時の order_status, payment_date をセット
                                $Order->setOrderStatus($app['eccube.repository.master.order_status']->find(6)); //-- 6 = 入金済み
                                $Order->setPaymentDate(new \DateTime()); //-- 入金日時(Now)を設定（とりあえず$resultGMOPG['TranDate']と若干の差が出るかも・・）
                            }
                            catch(\Exception $ex){
                                return $this->_creditCardPaymentException(
                                    $app,
                                    $Order,
                                    $ex->GetMessage(),
                                    GmoPg::GMOPG_GetErrorMessage(
                                        $app,
                                        $ex->GetMessage()
                                    )
                                );
                            }
/*****
return $this->_creditCardPaymentException(
    $app,
    $Order,
    'E01060005|E01060010',
    GmoPg::GMOPG_GetErrorMessage(
        $app,
        'E01060005|E01060010'
    )
);
*****/
                        }

                        //-- 購入処理
                        $this->_execPurchase($app, clone $Order);

                        //-- 注文完了メール送信
                        $MailHistory = $app['eccube.service.shopping']->sendOrderMailAirAccess($Order, $resultGMOPG);
                    }
                    return $app->redirect($app->url('input_done'));
                }
            }
        }

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input.twig' : 'air_access_sp_input.twig';
        return $app->render($template, array(
            'form' => $form->createView(),
            'nonmember' => $nonmember,
            'payments' => $payments,
            'paymentSelected' => $paymentId,
            'token' => $token,
            'maskedCardNumber' => $maskedCardNumber,
        ));
    }

    //--------------------------------------------------------------------------------
    //-- 非会員
    //--------------------------------------------------------------------------------
    public function nonmember(Application $app, Request $request)
    {
        $builder = $app['form.factory']->createBuilder('air_access_member');
        $form = $builder->getForm();
        $form->handleRequest($request);

        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input.twig' : 'air_access_sp_input.twig';
        return $app->render($template, array(
            'form' => $form->createView(),
            'nonmember' => '1',
            'payments' => $this->_getPayments($app),
            'paymentSelected' => 0,
            'token' => '',
            'maskedCardNumber' => '',
        ));
    }

    //--------------------------------------------------------------------------------
    //-- パスワード認証完了（クレジット決済時のみ）
    //--------------------------------------------------------------------------------
    public function auth_done(Application $app, Request $request)
    {
        //-- 3D Auth スルーオブジェクトを取得
        $throughObj = $app['session']->get($this->session3dAuth_ThroughObjKey);    //dump($throughObj);
        //$Order = $throughObj['Order'];
        $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);
        //-- 領収書と備考をセット
        $Order->setReceipt($throughObj['receipt']);
        $Order->setMessage($throughObj['message']);

        //-- 支払い方法をセット
        $payments = $this->_getPayments($app);
        $selectedPayment = $this->_getSelectedPayment($payments, $throughObj['paymentId']); //dump($selectedPayment);
        //-- 選択された支払方法をセット
        $Order->setPayment($selectedPayment);
        $Order->setPaymentMethod($selectedPayment['Method']);
        $Order->setCharge($selectedPayment['Charge']);
//dump($Order);exit;

        //-- GMO PG 側からPOSTされたパラメータを取得
        $PaRes = $request->get('PaRes');
        $MD = $request->get('MD');

        try{
            $resultGMOPG = GmoPg::GMOPG_ExecTran_3D_AfterAuth($PaRes, $MD);    //dump($resultGMOPG);    exit;

            //-- GMO PG API からのレスポンスを dtb_order にセット
            $Order->setGMO_Approve($resultGMOPG['Approve']);
            $Order->setGMO_TranID($resultGMOPG['TranID']);
            $Order->setGMO_TranDate($resultGMOPG['TranDate']);
            $Order->setGMO_CheckSum($resultGMOPG['CheckString']);

            //-- クレジット決済時の order_status, payment_date をセット
            $Order->setOrderStatus($app['eccube.repository.master.order_status']->find(6)); //-- 6 = 入金済み
            $Order->setPaymentDate(new \DateTime()); //-- 入金日時(Now)を設定（とりあえず$resultGMOPG['TranDate']と若干の差が出るかも・・）

            //-- 購入処理
            $this->_execPurchase($app, clone $Order);

            //-- 注文完了メール送信
            $MailHistory = $app['eccube.service.shopping']->sendOrderMailAirAccess($Order, $resultGMOPG);

            return $app->redirect($app->url('input_done'));
        }
        catch(\Exception $ex){
            return $this->_creditCardPaymentException(
                $app,
                $Order,
                $ex->GetMessage(),
                GmoPg::GMOPG_GetErrorMessage(
                    $app,
                    $ex->GetMessage()
                )
            );
        }
    }

    //--------------------------------------------------------------------------------
    //-- お客様情報入力完了
    //--------------------------------------------------------------------------------
    public function done(Application $app, Request $request)
    {
        //-- 受注IDを取得
        $orderId = $app['session']->get($this->sessionOrderKey);    //dump($orderId);

        $TargetOrder = null;
        $TargetOrder = $app['eccube.repository.order']->find($orderId);
        if (is_null($TargetOrder)) {
            throw new NotFoundHttpException();
        }

        //-- 入金済みの場合、注文数分の優待番号を取得し返す
        if($TargetOrder['OrderStatus']['id'] == 6){
/*****
            foreach ($TargetOrder->getOrderDetails() as $OrderDetail) {
                $quantities[$OrderDetail->getId()] = $OrderDetail->getQuantity();
            }

            $benefits = BenefitUtil::GetBenefitForUpdate(
                $orderId,
                $this->getQuantity($quantities, $orderId)
            );

            if(($benefits != null) || (count($benefits) > 0)){
                BenefitUtil::DeliveredBenefit($orderId);

                //-- 優待番号メール送信
                $MailHistory = BenefitUtil::SendMail($app, $benefits, $TargetOrder);
            }
*****/

            foreach ($TargetOrder->getOrderDetails() as $OrderDetail) {
//dump($OrderDetail->getProductCode());
//dump($OrderDetail->getQuantity());
                $benefitsNew[$OrderDetail->getProductCode()] = BenefitUtil::GetBenefitForUpdateNew(
                    $orderId,
                    $OrderDetail->getProductCode(),
                    $OrderDetail->getQuantity()
                );
            }   //dump($benefitsNew);

            if($this->isValidSendBenefits($benefitsNew)){
                BenefitUtil::DeliveredBenefit($orderId);
                
                //-- 優待番号メール送信
                $MailHistory = BenefitUtil::SendMailNew($app, $benefitsNew, $TargetOrder);
            }
            else{
                BenefitUtil::RevertBenefit($orderId);
                
                //-- 優待番号空きなしメール送信
                $MailHistory = BenefitUtil::SendEmptyMail($app, $TargetOrder);
            }
        }




        //-- 受注に関連するセッションを削除
        $app['session']->remove($this->sessionOrderKey);
        $app['session']->remove($this->sessionMultipleKey);

        //-- 非会員用セッション情報を空の配列で上書きする(プラグイン互換性保持のために削除はしない)
        $app['session']->set($this->sessionKey, array());
        $app['session']->set($this->sessionCustomerAddressKey, array());

        //-- 3D Auth スルーオブジェクトセッションを削除
        $app['session']->remove($this->session3dAuth_ThroughObjKey);
        
        log_info('購入処理完了', array($orderId));

        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input_done.twig' : 'air_access_sp_input_done.twig';
        return $app->render($template, array(
            'orderId' => $orderId,
        ));
    }

    //--------------------------------------------------------------------------------
    //-- エラー
    //--------------------------------------------------------------------------------
    public function error(Application $app, Request $request)
    {
        //-- エラーコードを取得
        $errors = $app['session']->get($this->sessionGMOPG_ErrorsKey);   //dump($errors);
        //-- エラーメッセージを取得
        $errorMessages = $app['session']->get($this->sessionGMOPG_ErrorMessagesKey);   //dump($errorMessages);
        
        //-- 受注IDを取得
        $orderId = $app['session']->get($this->sessionOrderKey);    //dump($orderId);

        //-- 受注に関連するセッションを削除
        $app['session']->remove($this->sessionOrderKey);
        $app['session']->remove($this->sessionMultipleKey);

        //-- 非会員用セッション情報を空の配列で上書きする(プラグイン互換性保持のために削除はしない)
        $app['session']->set($this->sessionKey, array());
        $app['session']->set($this->sessionCustomerAddressKey, array());

        //-- 3D Auth スルーオブジェクトセッションを削除
        $app['session']->remove($this->session3dAuth_ThroughObjKey);
        
        //-- エラーコードセッションを削除
        $app['session']->remove($this->sessionGMOPG_ErrorsKey);
        //-- エラーメッセージセッションを削除
        $app['session']->remove($this->sessionGMOPG_ErrorMessagesKey);
        
        log_info('購入処理エラー', array($orderId));

        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_input_error.twig' : 'air_access_sp_input_error.twig';
        return $app->render($template, array(
            'orderId' => $orderId,
            'errors' => $errors,
            'errorMessages' => $errorMessages,
        ));
    }












    //--------------------------------------------------------------------------------
    //-- 数量を返す
    //--------------------------------------------------------------------------------
/*****
    private function getQuantity($quantities, $id)
    {
        foreach($quantities as $key => $value){
            if($key == $id){
                return $value;
            }
        }
        return 0;
    }
*****/

    //--------------------------------------------------------------------------------
    //-- メール送信する優待番号は取得出来ているか
    //--------------------------------------------------------------------------------
    private function isValidSendBenefits($benefits)
    {
        foreach($benefits as $benefit){
            if($benefit == null){
                return false;   //-- 複数の商品コードのうち、一つでも取得出来なかった場合は false を返す
            }
        }
        return true;
    }
}
