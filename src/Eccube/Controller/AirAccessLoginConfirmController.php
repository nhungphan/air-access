<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Symfony\Component\HttpFoundation\Request;

class AirAccessLoginConfirmController extends AbstractController
{
    /* カート画面からのログイン
    *
    */
    public function index(Application $app, Request $request)
    {
        $previous_url = $_SERVER['HTTP_REFERER'];

        if(empty($previous_url)){
          $pre_url = 'top';
        }else{
          if(strpos($previous_url, '/') !== false){
            $pre_array = explode('/', $previous_url);
            $pre_url = end($pre_array);
          }else{
            $pre_url = $previous_url;
          }
        }

        $session = $app['session'];

        $session->set('pre_url', $pre_url);

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_login_confirm.twig' : 'air_access_sp_login_confirm.twig';
        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        return $app->render($template);

    }

}
