<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Symfony\Component\HttpFoundation\Request;

class AirAccessTopController extends AbstractController
{

    public function index(Application $app, Request $request)
    {
        //-- 新着情報を取得（新着情報は管理画面の「コンテンツ管理」>「新着情報管理」で設定するものとする）
        $NewsList = $app['orm.em']->getRepository('\Eccube\Entity\News')
            ->findBy(
                array(),
                array('rank' => 'DESC')
            );
//dump($NewsList);

        $NoticeList = $app['orm.em']->getRepository('\Eccube\Entity\Notice')
        ->findBy(
            array(),
            array('rank' => 'DESC')
        );
//dump($NoticeList);

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_top.twig' : 'air_access_sp_top.twig';

        $paths = array($app['config']['user_data_realdir']);         
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));
        return $app->render(
            $template,
            array(
                'NewsList' => $NewsList,    //-- 表示する可能性ある為、オブジェクトを渡しておく
            )
        );
    }
}
