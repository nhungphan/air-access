<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;
// use Eccube\Util\Costomer;

class AirAccessLoginFormController extends AbstractController
{


    /**
     * ログイン画面.
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Application $app, Request $request)
    {

        $session = $app['session'];

        $previous_url = $_SERVER['HTTP_REFERER'];
        $pre_url = '';

        if(strpos($previous_url, '/') !== false){
          $pre_array = explode('/', $previous_url);
          $pre_url = end($pre_array);
        }else{
          $pre_url = 'top';
        }
        $session->set('pre_url', $pre_url);

        if ($app->isGranted('IS_AUTHENTICATED_FULLY')) {
            log_info('認証済のためログイン処理をスキップ');

            return $app->redirect($app->url('mypage'));
        }

        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_login_form.twig' : 'air_access_sp_login_form.twig';

        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));

        return $app->render($template);

    }


    /*
    *ログインフォームに入力された値をget
    */

    public function getdata(Application $app, Request $request)
    {

      $email = $request->get('login_email', "");
      $pass = $request->get('login_pass', "");

      $session = $app['session'];

      // 入力内容チェック
      $err_flg = 0;
      $message = self::checkContents($email, $pass);

      if($message > 0){
        $err_flg = 1;
      }

      $res = \Eccube\Util\Customer::checkCustomer($email, $pass);

      // 顧客情報がない場合
      if(!count($res)){
        $err_flg = 1;
      }

      if($err_flg){
        $template = $this->isPC($request->headers->get('User-Agent')) ? 'air_access_login_form.twig' : 'air_access_sp_login_form.twig';
        $paths = array($app['config']['user_data_realdir']);
        $app['twig.loader']->addLoader(new \Twig_Loader_Filesystem($paths));
        return $app->render($template, array('err_flg' => $err_flg));
      }


      // 会員情報を画面に合わせて整形
      $email = "";
      if(isset($res->email) && !empty($res->email)){
        $email = $res->email;
      }else{
        $email = $res->email_mobile;
      };

      // sessionに積む
      $session->set(
        'customer', array(
        'id' => $res->customer_id,
        'name01' => $res->name01,
        'name02' => $res->name02,
        'kana01' => $res->kana01,
        'kana02' => $res->kana02,
        'email' => $email,
        'tel01' => $res->tel01,
        'tel02' => $res->tel02,
        'tel03' => $res->tel03,
      ));

      $previous_url = $session->get('pre_url');

      // login成功後の戻り値設定
      switch ($previous_url){
        case 'cart':
          return $app->redirect($app->url('cart'));
          break;

        case 'qa':
          return $app->redirect($app->url('qa'));
          break;

        case 'flow':
          return $app->redirect($app->url('flow'));
          break;

        case 'list':
          return $app->redirect($app->url('product_list'));
          break;

        case 'notes':
          return $app->redirect($app->url('notes'));
          break;

        case 'contact':
          return $app->redirect($app->url('contact'));
          break;

        case '':
          return $app->redirect($app->url('top'));
          break;

        default:
          return $app->redirect($app->url('air_access_login_form'));
          break;
        }

    }

    /**
     * 入力内容チェック
     *
     */
    public function checkContents($email, $pass)
    {
      $error_message = 0;

      if(!isset($email) && empty($email)){
        $error_message++;
      }
      if(!isset($pass) && empty($pass)){
        $error_message++;
      }

      return $error_message;
    }



    /**
     * マイページ
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {
        $Customer = $app['user'];

        /* @var $softDeleteFilter \Eccube\Doctrine\Filter\SoftDeleteFilter */
        $softDeleteFilter = $app['orm.em']->getFilters()->getFilter('soft_delete');
        $softDeleteFilter->setExcludes(array(
            'Eccube\Entity\ProductClass',
        ));

        // 購入処理中/決済処理中ステータスの受注を非表示にする.
        $app['orm.em']
            ->getFilters()
            ->enable('incomplete_order_status_hidden');

        // paginator
        $qb = $app['eccube.repository.order']->getQueryBuilderByCustomer($Customer);

        $event = new EventArgs(
            array(
                'qb' => $qb,
                'Customer' => $Customer,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_INDEX_SEARCH, $event);

        $pagination = $app['paginator']()->paginate(
            $qb,
            $request->get('pageno', 1),
            $app['config']['search_pmax']
        );

        return $app->render('Mypage/index.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * 購入履歴詳細を表示する.
     *
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function history(Application $app, Request $request, $id)
    {
        /* @var $softDeleteFilter \Eccube\Doctrine\Filter\SoftDeleteFilter */
        $softDeleteFilter = $app['orm.em']->getFilters()->getFilter('soft_delete');
        $softDeleteFilter->setExcludes(array(
            'Eccube\Entity\ProductClass',
        ));

        $app['orm.em']->getFilters()->enable('incomplete_order_status_hidden');
        $Order = $app['eccube.repository.order']->findOneBy(array(
            'id' => $id,
            'Customer' => $app->user(),
        ));

        $event = new EventArgs(
            array(
                'Order' => $Order,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_HISTORY_INITIALIZE, $event);

        $Order = $event->getArgument('Order');

        if (!$Order) {
            throw new NotFoundHttpException();
        }

        return $app->render('Mypage/history.twig', array(
            'Order' => $Order,
        ));
    }

    /**
     * 再購入を行う.
     *
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function order(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        log_info('再注文開始', array($id));

        $Customer = $app->user();

        /* @var $Order \Eccube\Entity\Order */
        $Order = $app['eccube.repository.order']->findOneBy(array(
            'id' => $id,
            'Customer' => $Customer,
        ));

        $event = new EventArgs(
            array(
                'Order' => $Order,
                'Customer' => $Customer,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_ORDER_INITIALIZE, $event);

        if (!$Order) {
            log_info('対象の注文が見つかりません', array($id));
            throw new NotFoundHttpException();
        }

        foreach ($Order->getOrderDetails() as $OrderDetail) {
            try {
                if ($OrderDetail->getProduct() &&
                    $OrderDetail->getProductClass()) {
                    $app['eccube.service.cart']->addProduct($OrderDetail->getProductClass()->getId(), $OrderDetail->getQuantity())->save();
                } else {
                    log_info($app->trans('cart.product.delete'), array($id));
                    $app->addRequestError('cart.product.delete');
                }
            } catch (CartException $e) {
                log_info($e->getMessage(), array($id));
                $app->addRequestError($e->getMessage());
            }
        }

        $event = new EventArgs(
            array(
                'Order' => $Order,
                'Customer' => $Customer,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_ORDER_COMPLETE, $event);

        if ($event->getResponse() !== null) {
            return $event->getResponse();
        }

        log_info('再注文完了', array($id));

        return $app->redirect($app->url('cart'));
    }

    /**
     * お気に入り商品を表示する.
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function favorite(Application $app, Request $request)
    {
        $BaseInfo = $app['eccube.repository.base_info']->get();

        if ($BaseInfo->getOptionFavoriteProduct() == Constant::ENABLED) {
            $Customer = $app->user();

            // paginator
            $qb = $app['eccube.repository.customer_favorite_product']->getQueryBuilderByCustomer($Customer);

            $event = new EventArgs(
                array(
                    'qb' => $qb,
                    'Customer' => $Customer,
                ),
                $request
            );
            $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_FAVORITE_SEARCH, $event);

            $pagination = $app['paginator']()->paginate(
                $qb,
                $request->get('pageno', 1),
                $app['config']['search_pmax'],
                array('wrap-queries' => true)
            );

            return $app->render('Mypage/favorite.twig', array(
                'pagination' => $pagination,
            ));
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * お気に入り商品を削除する.
     *
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        $Customer = $app->user();

        $Product = $app['eccube.repository.product']->find($id);

        $event = new EventArgs(
            array(
                'Customer' => $Customer,
                'Product' => $Product,
            ), $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_DELETE_INITIALIZE, $event);

        if ($Product) {
            log_info('お気に入り商品削除開始');

            $app['eccube.repository.customer_favorite_product']->deleteFavorite($Customer, $Product);

            $event = new EventArgs(
                array(
                    'Customer' => $Customer,
                    'Product' => $Product,
                ), $request
            );
            $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_MYPAGE_MYPAGE_DELETE_COMPLETE, $event);

            log_info('お気に入り商品削除完了');
        }

        return $app->redirect($app->url('mypage_favorite'));
    }
}
