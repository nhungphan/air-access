<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170829161951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if ($this->connection->getDatabasePlatform()->getName() == 'mysql') {
            if (!$schema->hasTable('dtb_news_image')) {
                $this->addSql(
                    'CREATE TABLE `dtb_news_image` ('
                        . '`news_image_id` int(11) NOT NULL,'
                        . '`news_id` int(11) NOT NULL,'
                        . '`creator_id` int(11) NOT NULL,'
                        . '`file_name` longtext NOT NULL,'
                        . '`rank` int(11) NOT NULL,'
                        . '`create_date` datetime NOT NULL'
                    . ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_news_image`'
                    . 'ADD PRIMARY KEY (`news_image_id`),'
                    . 'ADD KEY `IDX_news_image_notice` (`news_id`),'
                    . 'ADD KEY `IDX_news_image_creator` (`creator_id`);'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_news_image`'
                    . 'MODIFY `news_image_id` int(11) NOT NULL AUTO_INCREMENT;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_news_image`'
                    . 'ADD CONSTRAINT `FK_news_image_creator` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`member_id`),'
                    . 'ADD CONSTRAINT `FK_news_image_notice` FOREIGN KEY (`news_id`) REFERENCES `dtb_news` (`news_id`);'
                );
            }

            if (!$schema->hasTable('dtb_notice')) {
                $this->addSql(
                    'CREATE TABLE `dtb_notice` ('
                        . '`notice_id` int(11) NOT NULL,'
                        . '`creator_id` int(11) NOT NULL,'
                        . '`notice_date` datetime DEFAULT NULL,'
                        . '`rank` int(11) DEFAULT NULL,'
                        . '`notice_title` longtext NOT NULL,'
                        . '`notice_comment` longtext,'
                        . '`notice_url` longtext,'
                        . '`notice_select` smallint(6) NOT NULL DEFAULT '0','
                        . '`link_method` smallint(6) NOT NULL DEFAULT '0','
                        . '`create_date` datetime NOT NULL,'
                        . '`update_date` datetime NOT NULL,'
                        . '`del_flg` smallint(6) NOT NULL DEFAULT '0''
                    . ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice`'
                    . 'ADD PRIMARY KEY (`notice_id`),'
                    . 'ADD KEY `IDX_notices` (`creator_id`);'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice`'
                    . 'MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice`'
                    . 'ADD CONSTRAINT `FK_notices` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`member_id`);'
                );
            }

            if (!$schema->hasTable('dtb_notice_image')) {
                $this->addSql(
                    'CREATE TABLE `dtb_notice_image` ('
                    . '`notice_image_id` int(11) NOT NULL,'
                    . '`notice_id` int(11) NOT NULL,'
                    . '`creator_id` int(11) NOT NULL,'
                    . '`file_name` longtext NOT NULL,'
                    . '`rank` int(11) NOT NULL,'
                    . '`create_date` datetime NOT NULL'
                    . ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice_image`'
                    . 'ADD PRIMARY KEY (`notice_image_id`),'
                    . 'ADD KEY `IDX_notice_image_notice` (`notice_id`),'
                    . 'ADD KEY `IDX_notice_image_creator` (`creator_id`);'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice_image`'
                    . 'MODIFY `notice_image_id` int(11) NOT NULL AUTO_INCREMENT;'
                );
                $this->addSql(
                    'ALTER TABLE `dtb_notice_image`'
                    . 'ADD CONSTRAINT `FK_notice_image_creator` FOREIGN KEY (`creator_id`) REFERENCES `dtb_member` (`member_id`),'
                    . 'ADD CONSTRAINT `FK_notice_image_notice` FOREIGN KEY (`notice_id`) REFERENCES `dtb_notice` (`notice_id`);'
                );
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        if ($this->connection->getDatabasePlatform()->getName() == 'mysql') {
            $this->addSql('DROP TABLE IF EXISTS `dtb_notice_image`;');
            $this->addSql('DROP TABLE IF EXISTS `dtb_notice`;');
            $this->addSql('DROP TABLE IF EXISTS `dtb_news_image`;');
        }
    }
}
