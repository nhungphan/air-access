<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170809170316 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $table = $schema->getTable('dtb_order');

        if(!$table->hasColumn('access_ticket_customer_id')){
            $this->addSql('ALTER TABLE dtb_order ADD access_ticket_customer_id integer null AFTER customer_id;');
//            $table->addColumn('access_ticket_customer_id', 'integer', array(
//                'NotNull' => false,
//                'after' => 'customer_id',
//                'default' => null
//            ));
        }
        if(!$table->hasColumn('receipt')){
            $this->addSql('ALTER TABLE dtb_order ADD receipt smallint not null default 0 AFTER pre_order_id;');
//            $table->addColumn('receipt', 'smallint', array(
//                'NotNull' => true,
//                'after' => 'pre_order_id',
//                'default' => 0
//            ));
        }
        if(!$table->hasColumn('gmo_approve')){
            $this->addSql('ALTER TABLE dtb_order ADD gmo_approve text null AFTER payment_date;');
//            $table->addColumn('gmo_approve', 'text', array(
//                'NotNull' => false,
//                'after' => 'payment_date',
//                'default' => null
//            ));
        }
        if(!$table->hasColumn('gmo_tran_id')){
            $this->addSql('ALTER TABLE dtb_order ADD gmo_tran_id text null AFTER gmo_approve;');
//            $table->addColumn('gmo_tran_id', 'text', array(
//                'NotNull' => false,
//                'after' => 'gmo_approve',
//                'default' => null
//            ));
        }
        if(!$table->hasColumn('gmo_tran_date')){
            $this->addSql('ALTER TABLE dtb_order ADD gmo_tran_date text null AFTER gmo_tran_id;');
//            $table->addColumn('gmo_tran_date', 'text', array(
//                'NotNull' => false,
//                'after' => 'gmo_tran_id',
//                'default' => null
//            ));
        }
        if(!$table->hasColumn('gmo_check_sum')){
            $this->addSql('ALTER TABLE dtb_order ADD gmo_check_sum text null AFTER gmo_tran_date;');
//            $table->addColumn('gmo_check_sum', 'text', array(
//                'After' => 'gmo_tran_date',
//                'NotNull' => false,
//                'Default' => null
//            ));
        }
        if(!$table->hasColumn('gmo_errors')){
            $this->addSql('ALTER TABLE dtb_order ADD gmo_errors text null AFTER gmo_check_sum;');
//            $table->addColumn('gmo_errors', 'text', array(
//                'NotNull' => false,
//                'after' => 'gmo_check_sum',
//                'default' => null
//            ));
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $table = $schema->getTable('dtb_order');

        if($table->hasColumn('receipt')){
            $table->dropColumn('receipt');
        }
        if($table->hasColumn('gmo_approve')){
            $table->dropColumn('gmo_approve');
        }
        if($table->hasColumn('gmo_tran_id')){
            $table->dropColumn('gmo_tran_id');
        }
        if($table->hasColumn('gmo_tran_date')){
            $table->dropColumn('gmo_tran_date');
        }
        if($table->hasColumn('gmo_check_sum')){
            $table->dropColumn('gmo_check_sum');
        }
        if($table->hasColumn('gmo_errors')){
            $table->dropColumn('gmo_errors');
        }
    }
}
