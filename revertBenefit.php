#!/usr/bin/php -q
<?php
  require_once ('/var/www/air-access/lib/DatabaseAccessMysql.php');

  $item = $argv[1]; // var_dump($item);
  $data = json_decode($item, true); // var_dump($data);

  //-- パラメータ取得
  $orderId = $data["orderId"];

  $dbam = DatabaseAccessMysql::singleton("mysql");
  $dbam->connectToDb();

  //-- トランザクション開始
  $dbam->execSql('begin', array());

  //-- order_id に対応した優待番号を解放する
  try{
    $res = $dbam->execSql(
      'update `dtb_benefit` set `order_id` = null, `done_date` = null where `order_id` = :orderId and `done_date` is not null and `del_flg` <> 1',
      array(':orderId' => $orderId)
    );
    $dbam->execSql('commit', array());
  }
  catch(Exception $ex){
    $dbam->execSql('rollback', array());
    $res = null;
  }

  exit(json_encode($res));
?>
